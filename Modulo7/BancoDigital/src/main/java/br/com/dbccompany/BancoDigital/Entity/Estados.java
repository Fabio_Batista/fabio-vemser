package br.com.dbccompany.BancoDigital.Entity;

import javax.persistence.*;

@Entity
@Table( name = "ESTADO")
public class Estados {
    @Id
    @SequenceGenerator( allocationSize = 1, name = "ESTADO_SEQ", sequenceName = "ESTADO_SEQ")
    @GeneratedValue( generator = "ESTADO_SEQ", strategy = GenerationType.SEQUENCE)

    private Integer id;

    @Column( name = "NOME", nullable = false, length = 60, unique = true)
    private String nome;

    @ManyToOne( cascade = CascadeType.ALL)
    @JoinColumn(name = "ID_PAIS")
    private Paises pais;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Paises getPais() {
        return pais;
    }

    public void setPais(Paises pais) {
        this.pais = pais;
    }
}
