package br.com.dbccompany.BancoDigital.Repository;

import br.com.dbccompany.BancoDigital.Entity.Banco;
import br.com.dbccompany.BancoDigital.Entity.Cidades;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CidadesRepository extends CrudRepository<Cidades, Integer > {
    List<Cidades> findAllByNome(String nome );
}
