package br.com.dbccompany.BancoDigital.Controller;

import br.com.dbccompany.BancoDigital.Service.BancoService;
import br.com.dbccompany.BancoDigital.Service.CidadesService;
import br.com.dbccompany.BancoDigital.Entity.Banco;
import br.com.dbccompany.BancoDigital.Entity.Cidades;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/cidades" )
public class CidadesController {

    @Autowired
    CidadesService service;

    @GetMapping( value = "/todas" )
    @ResponseBody
    public List<Cidades> todasCidades() {
        return service.todasCidades();
    }

    @GetMapping ( value = "/todas/{id}")
    @ResponseBody
    public Cidades cidadePorId( @PathVariable Integer id ){
        return service.cidadeEspecifica(id);
    }

    @GetMapping ( value = "/todas/{nome}")
    @ResponseBody
    public List<Cidades> cidadePorNome( @PathVariable String nome ){
        return service.cidadesPorNome(nome);
    }


    @PostMapping( value = "/nova" )
    @ResponseBody
    public Cidades novaCidade( @RequestBody Cidades cidade) {
        return service.salvar(cidade);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public  Cidades editarCidade( @PathVariable Integer id, @RequestBody Cidades cidade ) {
        return service.editar(cidade, id);
    }


}
