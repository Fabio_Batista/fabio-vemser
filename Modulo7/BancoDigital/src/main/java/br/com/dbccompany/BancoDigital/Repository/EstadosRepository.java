package br.com.dbccompany.BancoDigital.Repository;

import br.com.dbccompany.BancoDigital.Entity.Estados;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EstadosRepository extends CrudRepository <Estados, Integer> {

    List <Estados> findAllByNome( String nome );

}
