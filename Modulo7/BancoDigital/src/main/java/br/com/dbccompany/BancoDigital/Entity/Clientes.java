package br.com.dbccompany.BancoDigital.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table( name = "CLIENTE")
public class Clientes {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "CLIENTE_SEQ", sequenceName = "CLIENTE_SEQ")
    @GeneratedValue( generator = "CLIENTE_SEQ", strategy = GenerationType.SEQUENCE)


    private Integer id;

    @Column( name = "NOME", nullable = false, length = 50)
    private String nome;

    @Column( name = "CPF", nullable = false, length = 14, unique = true)
    private String cpf;

    @ManyToMany( cascade = CascadeType.ALL)
    @JoinTable( name = "CIDADE_CLIENTE",
        joinColumns = { @JoinColumn(name = "ID_CLIENTE")},
        inverseJoinColumns = { @JoinColumn( name = "ID_CIDADE") } )
    private List<Cidades> cidades = new ArrayList<>();

    @ManyToMany( cascade =  CascadeType.ALL )
    @JoinTable ( name = "CONTA_CLIENTE",
        joinColumns = { @JoinColumn( name = "ID_CLIENTE")},
        inverseJoinColumns = { @JoinColumn( name = "ID_CONTA") } )
    private List<Contas> contas = new ArrayList<>();
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public List<Cidades> getCidades() {
        return cidades;
    }

    public void setCidades(List<Cidades> cidades) {
        this.cidades = cidades;
    }

    public List<Contas> getContas() {
        return contas;
    }

    public void setContas(List<Contas> contas) {
        this.contas = contas;
    }
}
