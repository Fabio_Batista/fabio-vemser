package br.com.dbccompany.BancoDigital.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "CONTA")
public class Contas {
    @Id
    @SequenceGenerator( allocationSize = 1, name = "CONTA_SEQ", sequenceName = "CONTA_SEQ")
    @GeneratedValue( generator = "CONTA_SEQ", strategy = GenerationType.SEQUENCE)


    private Integer id;

    @Column( name = "NOME", nullable = false, length = 50, unique = true)
    private String nome;

    @Column( name = "CONTA", nullable = false, unique = true)
    private Integer conta;

    @Column( name= "SALDO", nullable = false )
    private Double saldo = 0.00;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "ID_TIPO_CONTA" )
    private TiposContas tipoConta;


    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "ID_AGENCIA" )
    private Agencias agencia;

    @ManyToMany( cascade =  CascadeType.ALL )
    @JoinTable ( name = "CONTA_CLIENTE",
            joinColumns = { @JoinColumn( name = "ID_CONTA")},
            inverseJoinColumns = { @JoinColumn( name = "ID_CLIENTE") } )
    private List<Clientes> clientes = new ArrayList<>();


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getConta() {
        return conta;
    }

    public void setConta(Integer conta) {
        this.conta = conta;
    }

    public Double getSaldo() {
        return saldo;
    }

    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    }

    public TiposContas getTipoConta() {
        return tipoConta;
    }

    public void setTipoConta(TiposContas tipoConta) {
        this.tipoConta = tipoConta;
    }

    public Agencias getAgencia() {
        return agencia;
    }

    public void setAgencia(Agencias agencia) {
        this.agencia = agencia;
    }

    public List<Clientes> getClientes() {
        return clientes;
    }

    public void setClientes(List<Clientes> clientes) {
        this.clientes = clientes;
    }
}
