package br.com.dbccompany.BancoDigital.Service;

import br.com.dbccompany.BancoDigital.Repository.TipoContaRepository;
import br.com.dbccompany.BancoDigital.Entity.TiposContas;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TipoContaService {

    @Autowired
    private TipoContaRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public TiposContas salvar(TiposContas tipoConta) {
        return repository.save(tipoConta);
    }

    @Transactional(rollbackFor = Exception.class)
    public TiposContas editar(TiposContas tipoConta, Integer id) {
        tipoConta.setId(id);
        return repository.save(tipoConta);
    }

    public List<TiposContas> todosTiposContas() {
        return (List<TiposContas>) repository.findAll();
    }

    public TiposContas buscaPorId( Integer id ) {
        return repository.findById(id).get();
    }

}
