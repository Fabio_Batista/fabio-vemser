package br.com.dbccompany.BancoDigital.Entity;

public enum TipoMovimentacao {
    DEBITO,
    CREDITO
}
