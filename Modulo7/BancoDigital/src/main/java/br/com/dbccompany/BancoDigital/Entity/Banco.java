package br.com.dbccompany.BancoDigital.Entity;

import javax.persistence.*;

@Entity
@Table( name = "BANCO")
public class Banco {
    @Id
    @SequenceGenerator( allocationSize = 1, name ="BANCO_SEQ", sequenceName = "BANCO_SEQ")
    @GeneratedValue( generator = "BANCO_SEQ", strategy = GenerationType.SEQUENCE)


    private Integer id;

    @Column( name = "NOME", nullable = false, unique = true)
    private String nome;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
