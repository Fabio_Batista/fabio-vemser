package br.com.dbccompany.BancoDigital.Repository;

import br.com.dbccompany.BancoDigital.Entity.Clientes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Repository
public interface ClientesRepository extends CrudRepository<Clientes, Integer> {
    List<Clientes> findAllByNome( String nome );
    List<Clientes> findAllByCpf( String cpf );
}
