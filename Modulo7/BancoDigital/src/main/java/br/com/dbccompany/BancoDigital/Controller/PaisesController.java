package br.com.dbccompany.BancoDigital.Controller;

import br.com.dbccompany.BancoDigital.Service.PaisesService;
import br.com.dbccompany.BancoDigital.Entity.Paises;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/paises")
public class PaisesController {
    @Autowired
    PaisesService service;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<Paises> buscarTodos() {
        return service.todosPaises();
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public Paises buscarId(@PathVariable Integer id) {
        return service.buscaPorId(id);
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Paises adicionar(@RequestBody Paises pais) {
        return service.salvar(pais);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Paises editar(@PathVariable Integer id, @RequestBody Paises pais) {
        return service.editar(pais, id);
    }


}
