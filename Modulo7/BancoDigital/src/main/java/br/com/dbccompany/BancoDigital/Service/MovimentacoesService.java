package br.com.dbccompany.BancoDigital.Service;

import br.com.dbccompany.BancoDigital.Repository.MovimentacoesRepository;
import br.com.dbccompany.BancoDigital.Entity.Banco;
import br.com.dbccompany.BancoDigital.Entity.Movimentacoes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class MovimentacoesService {

    @Autowired
    MovimentacoesRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public Movimentacoes salvar(Movimentacoes mov ) {
        return repository.save(mov);
    }

    @Transactional ( rollbackFor = Exception.class )
    public Movimentacoes editar( Movimentacoes mov, Integer id ) {
        mov.setId(id);
        return repository.save(mov);
    }

    public List<Movimentacoes> todosMovimentacoes() {
        return (List<Movimentacoes>) repository.findAll();
    }

    public Movimentacoes movEspecifico( Integer id ) {
        Optional<Movimentacoes> mov = repository.findById(id);
        return mov.get();
    }

}
