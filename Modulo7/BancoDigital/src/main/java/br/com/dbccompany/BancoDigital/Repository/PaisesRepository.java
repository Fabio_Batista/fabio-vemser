package br.com.dbccompany.BancoDigital.Repository;


import br.com.dbccompany.BancoDigital.Entity.Paises;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PaisesRepository extends CrudRepository<Paises, Integer> {
    List<Paises> findAllByNome(String nome );
}