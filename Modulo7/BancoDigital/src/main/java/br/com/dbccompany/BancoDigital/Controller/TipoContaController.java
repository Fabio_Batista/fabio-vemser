package br.com.dbccompany.BancoDigital.Controller;

import br.com.dbccompany.BancoDigital.Service.TipoContaService;
import br.com.dbccompany.BancoDigital.Entity.TiposContas;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/tipoconta")
public class TipoContaController {
    @Autowired
    TipoContaService service;

    @GetMapping(value = "/todas")
    @ResponseBody
    public List<TiposContas> buscarTodas() {
        return service.todosTiposContas();
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public TiposContas buscarId(@PathVariable Integer id) {
        return service.buscaPorId(id);
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public TiposContas adicionar(@RequestBody TiposContas tipoConta) {
        return service.salvar(tipoConta);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public TiposContas editar(@PathVariable Integer id, @RequestBody TiposContas tipoConta) {
        return service.editar(tipoConta, id);
    }

}
