package br.com.dbccompany.BancoDigital.Repository;


import br.com.dbccompany.BancoDigital.Entity.TiposContas;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TipoContaRepository extends CrudRepository<TiposContas, Integer > {
    List<TiposContas> findAllByNome(String nome );
}
