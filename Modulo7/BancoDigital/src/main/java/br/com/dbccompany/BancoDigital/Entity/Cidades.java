package br.com.dbccompany.BancoDigital.Entity;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table( name = "CIDADE" )
public class Cidades {
    @Id
    @SequenceGenerator( allocationSize = 1, name = "CIDADE_SEQ", sequenceName = "CIDADE_SEQ" )
    @GeneratedValue( generator = "CIDADE_SEQ", strategy = GenerationType.SEQUENCE )


    private Integer id;

    @Column( name = "NOME", nullable = false, unique = true)
    private String nome;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "ID_CIDADE")
    private Cidades cidade;

    @ManyToMany( cascade = CascadeType.ALL)
    @JoinTable( name = "CIDADE_CLIENTE",
        joinColumns = { @JoinColumn( name = "ID_CIDADE")},
        inverseJoinColumns = {@JoinColumn( name = "ID_CLIENTE")})
    private List<Clientes> clientes = new ArrayList<>();


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Cidades getCidade() {
        return cidade;
    }

    public void setCidade(Cidades cidade) {
        this.cidade = cidade;
    }

    public List<Clientes> getClientes() {
        return clientes;
    }

    public void setClientes(List<Clientes> clientes) {
        this.clientes = clientes;
    }
}
