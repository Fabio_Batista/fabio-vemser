package br.com.dbccompany.BancoDigital.Service;

import br.com.dbccompany.BancoDigital.Repository.AgenciasRepository;
import br.com.dbccompany.BancoDigital.Entity.Agencias;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;


@Service
public class AgenciasService {

    @Autowired
    AgenciasRepository repository;

    @Transactional ( rollbackFor = Exception.class )
    public Agencias salvar( Agencias agencia ) {
        return repository.save(agencia);
    }

    @Transactional ( rollbackFor = Exception.class )
    public Agencias editar( Integer codigo, Agencias agencia ) {
        agencia.setId(codigo);
        return repository.save(agencia);
    }

    public List<Agencias> todasAgencias()
    {
        return (List<Agencias>) repository.findAll();
    }

    public Agencias agenciaEspecifica( Integer codigo ) {
        Optional<Agencias> agencia =  repository.findById(codigo);
        return agencia.get();
    }

    public List<Agencias> agenciaPorNome( String nome ){
        return repository.findAllByNome(nome);
    }
}
