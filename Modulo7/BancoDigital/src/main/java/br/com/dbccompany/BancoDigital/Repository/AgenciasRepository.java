package br.com.dbccompany.BancoDigital.Repository;

import br.com.dbccompany.BancoDigital.Entity.Agencias;
import br.com.dbccompany.BancoDigital.Entity.Banco;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AgenciasRepository extends CrudRepository<Agencias, Integer > {

    List<Agencias> findAllByNome( String nome );

}
