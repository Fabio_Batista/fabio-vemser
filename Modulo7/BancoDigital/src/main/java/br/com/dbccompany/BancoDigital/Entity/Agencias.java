package br.com.dbccompany.BancoDigital.Entity;

import javax.persistence.*;

@Entity
@Table( name = "AGENCIA")
public class Agencias {
    @Id
    @SequenceGenerator( allocationSize = 1, name = "AGENCIA_SEQ", sequenceName = "AGENCA_SEQ")
    @GeneratedValue( generator = "AGENCIA_SEQ", strategy = GenerationType.SEQUENCE )


    @Column( name = "ID_CODIGO", nullable = false, unique = true)
    private Integer id;

    @Column( name = "NOME", nullable = false, length = 50, unique = true)
    private String nome;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "ID_BANCO" )
    private Banco banco;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "ID_CIDADE" )
    private Cidades cidade;

    public Banco getBanco() {
        return banco;
    }

    public void setBanco(Banco banco) {
        this.banco = banco;
    }

    public Cidades getCidade() {
        return cidade;
    }

    public void setCidade(Cidades cidade) {
        this.cidade = cidade;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

}
