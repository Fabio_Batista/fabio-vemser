package br.com.dbccompany.BancoDigital.Repository;

import br.com.dbccompany.BancoDigital.Entity.Movimentacoes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MovimentacoesRepository extends CrudRepository<Movimentacoes, Integer> {

}
