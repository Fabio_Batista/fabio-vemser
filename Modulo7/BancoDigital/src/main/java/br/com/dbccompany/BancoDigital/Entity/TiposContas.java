package br.com.dbccompany.BancoDigital.Entity;

import javax.persistence.*;

@Entity
@Table( name = "TIPO_CONTA")
public class TiposContas {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "TIPO_CONTA_SEQ", sequenceName = "TIPO_CONTA_SEQ")
    @GeneratedValue( generator = "TIPO_CONTA_SEQ",strategy = GenerationType.SEQUENCE)

    private Integer id;

    @Column( name = "NOME", nullable = false, length = 40)
    private String nome;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

}
