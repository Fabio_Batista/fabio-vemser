package br.com.dbccompany.BancoDigital.Controller;


import br.com.dbccompany.BancoDigital.Service.CidadesService;
import br.com.dbccompany.BancoDigital.Service.ClientesService;
import br.com.dbccompany.BancoDigital.Entity.Cidades;
import br.com.dbccompany.BancoDigital.Entity.Clientes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/clientes" )
public class ClientesController {

    @Autowired
    ClientesService service;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<Clientes> todosClientes() {
        return service.todosClientes();
    }

    @GetMapping ( value = "/todos/{id}")
    @ResponseBody
    public Clientes clientePorId( @PathVariable Integer id ){
        return service.clienteEspecifico(id);
    }

    @GetMapping ( value = "/todos/{nome}")
    @ResponseBody
    public List<Clientes> clientePorNome( @PathVariable String nome ){
        return service.clientesPorNome(nome);
    }

    @GetMapping ( value = "/todos/{cpf}")
    @ResponseBody
    public List<Clientes> clientePorCpf( @PathVariable String cpf ){
        return service.clientesPorNome(cpf);
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public Clientes novoCliente( @RequestBody Clientes cliente) {
        return service.salvar(cliente);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public  Clientes editarCliente( @PathVariable Integer id, @RequestBody Clientes cliente ) {
        return service.editar(cliente, id);
    }
}
