package br.com.dbccompany.BancoDigital.Entity;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;

@Entity
@Table( name = "PAIS")
public class Paises {
    @Id
    @SequenceGenerator( allocationSize = 1, name = "PAIS_SEQ", sequenceName = "PAIS_SEQ")
    @GeneratedValue( generator = "PAIS_SEQ", strategy = GenerationType.SEQUENCE)

    private Integer id;

    @Column( name = "NOME", nullable = false, length= 60, unique = true)
    private String nome;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

}
