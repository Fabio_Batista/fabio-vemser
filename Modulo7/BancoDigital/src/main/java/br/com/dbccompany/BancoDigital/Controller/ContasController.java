package br.com.dbccompany.BancoDigital.Controller;

import br.com.dbccompany.BancoDigital.Service.ClientesService;
import br.com.dbccompany.BancoDigital.Service.ContasService;
import br.com.dbccompany.BancoDigital.Entity.Clientes;
import br.com.dbccompany.BancoDigital.Entity.Contas;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/contas" )
public class ContasController {

    @Autowired
    ContasService service;

    @GetMapping(value = "/todas")
    @ResponseBody
    public List<Contas> todasContas() {
        return service.todasContas();
    }

    @GetMapping ( value = "/todas/{id}")
    @ResponseBody
    public Contas contaPorId( @PathVariable Integer id ){
        return service.contaEspecifico(id);
    }

    @GetMapping ( value = "/todas/{numero}")
    @ResponseBody
    public Contas contaPorNumero( @PathVariable Integer numero ){
        return service.contasPorNumero(numero);
    }

    @PostMapping( value = "/nova" )
    @ResponseBody
    public Contas novaConta( @RequestBody Contas conta) {
        return service.salvar(conta);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public  Contas editarConta( @PathVariable Integer id, @RequestBody Contas conta ) {
        return service.editar(conta, id);
    }



}