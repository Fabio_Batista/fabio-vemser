package br.com.dbccompany.BancoDigital.Repository;

import br.com.dbccompany.BancoDigital.Entity.Contas;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContasRepository extends CrudRepository <Contas, Integer> {
    Contas findByNumero( Integer numero );
}
