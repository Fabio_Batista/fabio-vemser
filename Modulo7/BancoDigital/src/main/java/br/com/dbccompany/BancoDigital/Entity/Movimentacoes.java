package br.com.dbccompany.BancoDigital.Entity;



import javax.persistence.*;

@Entity
@Table( name = "MOVIMENTACAO")
public class Movimentacoes {
    @Id
    @SequenceGenerator( allocationSize = 1, name = "MOVIMENTACAO_SEQ", sequenceName = "MOVIMENTACAO_SEQ" )
    @GeneratedValue( generator = "MOVIMENTACAO_SEQ", strategy = GenerationType.SEQUENCE )

    private Integer id;

    @Column( name = "VALOR", nullable = false )
    private Double valor;

    @Enumerated( EnumType.STRING )
    private TipoMovimentacao tipoMovimento;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "ID_CONTA")
    private Contas conta;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public TipoMovimentacao getTipoMovimento() {
        return tipoMovimento;
    }

    public void setTipoMovimento(TipoMovimentacao tipoMovimento) {
        this.tipoMovimento = tipoMovimento;
    }

    public Contas getConta() {
        return conta;
    }

    public void setConta(Contas conta) {
        this.conta = conta;
    }
}
