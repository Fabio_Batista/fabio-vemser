package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Acesso;
import br.com.dbccompany.coworking.Entity.TipoContato;
import br.com.dbccompany.coworking.Repository.TipoContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class TipoContatoService {
    @Autowired
    private TipoContatoRepository repository;

    @Transactional( rollbackFor = Exception.class)
    public TipoContato salvar( TipoContato tipoContato ){
        return repository.save(tipoContato);
    }
    @Transactional(rollbackFor = Exception.class)
    public TipoContato editar( Integer id, TipoContato tipoContato ){
        tipoContato.setId(id);
        return repository.save(tipoContato);
    }
    @Transactional(rollbackFor = Exception.class)
    public void deletar( Integer id ){
        repository.deleteById(id);
    }

    public List<TipoContato> todosTiposContatos(){
        return repository.findAll();
    }
    public TipoContato tipoContatoEspecifico(Integer id){
        Optional<TipoContato> tipoContato = repository.findById(id);
        return tipoContato.get();
    }
    public TipoContato nomeEspecifico( String nome ) {
        return repository.findByNome( nome );
    }
    public List<TipoContato> tipoContatoPorNome( String nome ) {
        return repository.findAllByNome( nome );
    }

}
