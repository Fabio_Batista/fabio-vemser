package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table( name = "TIPOS_CONTATOS")
public class TipoContato {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "TIPO_CONTATO_SEQ", sequenceName = "TIPO_CONTATO_SEQ")
    @GeneratedValue(generator = "TIPO_CONTATO_SEQ", strategy = GenerationType.SEQUENCE)

    @Column( name = "ID_TIPO_CONTATO", nullable = false)
    private Integer id;

    @Column( name = "NOME",nullable = false, length = 50)
    private String nome;

    @OneToMany( mappedBy = "tipoContato" )
    private List<Contato> contatos = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
