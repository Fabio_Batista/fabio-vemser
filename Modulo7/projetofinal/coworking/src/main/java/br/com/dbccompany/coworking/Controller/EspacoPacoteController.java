package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.EspacoPacote;
import br.com.dbccompany.coworking.Entity.TipoContratacao;
import br.com.dbccompany.coworking.Service.EspacoPacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "/api/espacopacote")
public class EspacoPacoteController {

    @Autowired
    private EspacoPacoteService service;

    @PostMapping(value = "/novo")
    @ResponseBody
    public EspacoPacote adicionarEspacoPacote(@RequestBody EspacoPacote espacoPacote) {
        return service.salvar(espacoPacote);
    }
    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public EspacoPacote editarEspacoPacote(@PathVariable Integer id, @RequestBody EspacoPacote espacoPacote) {
        return service.editar( id, espacoPacote);
    }
    @RequestMapping( value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Boolean deletarEspacoPacote( @PathVariable Integer id ) {
        service.deletar( id );
        return true;
    }
    @GetMapping(value = "/{id}")
    @ResponseBody
    public EspacoPacote buscarId(@PathVariable Integer id) {
        return service.espacoPacoteEspecifico(id);
    }
    @GetMapping(value = "/todos")
    @ResponseBody
    public List<EspacoPacote> todosEspacoPacote(){
        return service.todosEspacoPacotes();
    }
    @GetMapping(value = "/tipocontratacao/{tipoContratacao}")
    @ResponseBody
    public EspacoPacote burcarTipoContratacao(@PathVariable TipoContratacao tipoContratacao){
        return service.tipoContratacaoEspecifico( tipoContratacao );
    }
    @GetMapping(value = "/qtd/{qtd}")
    @ResponseBody
    public EspacoPacote burcarQtd(@PathVariable Integer qtd){
        return service.quantidadeEspecifica( qtd );
    }
    @GetMapping(value = "/prazo/{prazo}")
    @ResponseBody
    public EspacoPacote burcarPrazo(@PathVariable Integer prazo ){
        return service.prazoEspecifico( prazo );
    }
}
