package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.EspacoPacote;
import br.com.dbccompany.coworking.Entity.TipoContratacao;
import br.com.dbccompany.coworking.Repository.EspacoPacoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class EspacoPacoteService {
    @Autowired
    private EspacoPacoteRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public EspacoPacote salvar(EspacoPacote espacoPacote){
        return repository.save(espacoPacote);
    }
    @Transactional(rollbackFor = Exception.class)
    public EspacoPacote editar(Integer id, EspacoPacote espacoPacote){
        espacoPacote.setId(id);
        return repository.save(espacoPacote);
    }
    @Transactional(rollbackFor = Exception.class)
    public void deletar(Integer id){
        repository.deleteById( id );
    }
    public List<EspacoPacote> todosEspacoPacotes(){
        return repository.findAll();
    }
    public EspacoPacote espacoPacoteEspecifico(Integer id){
        Optional<EspacoPacote> EspacoPacote = repository.findById(id);
        return EspacoPacote.get();
    }
    public EspacoPacote tipoContratacaoEspecifico( TipoContratacao tipoContratacao ) {
        return repository.findByTipoContratacao( tipoContratacao );
    }
    public EspacoPacote quantidadeEspecifica( Integer quantidade ) {
        return  repository.findByQuantidade( quantidade );
    }
    public EspacoPacote prazoEspecifico( Integer prazo ) {
        return repository.findByPrazo( prazo );
    }
}
