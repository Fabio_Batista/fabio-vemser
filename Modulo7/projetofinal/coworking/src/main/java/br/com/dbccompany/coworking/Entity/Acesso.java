package br.com.dbccompany.coworking.Entity;

import org.hibernate.procedure.spi.ParameterRegistrationImplementor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "ACESSOS")
public class Acesso {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "ACESSO_SEQ", sequenceName = "ACESSO_SEQ")
    @GeneratedValue( generator = "ACESSO_SEQ", strategy = GenerationType.SEQUENCE)

    @Column(name = "ID_ACESSO", nullable = false)
    private Integer id;

    @ManyToOne(cascade = CascadeType.ALL)
    @MapsId("ID_CLIENTE")
    @JoinColumn(name = "ID_SALDO_CLIENTE", nullable = false)
    private Cliente cliente;

    @ManyToOne(cascade = CascadeType.ALL)
    @MapsId("ID_ESPACO")
    @JoinColumn(name = "ID_ESPACO_CLIENTE", nullable = false)
    private Espaco espaco;

    @Column(name = "IS_ENTRADA", nullable = false)
    private Boolean is_entrada;

    @Column(name = "DATA", nullable = false)
    private Date data;

    @Column(name = "IS_EXCECAO")
    private Boolean is_excecao;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Espaco getEspaco() {
        return espaco;
    }

    public void setEspaco(Espaco espaco) {
        this.espaco = espaco;
    }

    public Boolean getEntrada() {
        return is_entrada;
    }

    public void setEntrada(Boolean is_entrada) {
        this.is_entrada = is_entrada;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Boolean getExcecao() {
        return is_excecao;
    }

    public void setExcecao(Boolean excecao) {
        this.is_excecao = excecao;
    }
}
