package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.SaldoCliente;
import br.com.dbccompany.coworking.Entity.SaldoClienteId;
import br.com.dbccompany.coworking.Entity.TipoContratacao;
import br.com.dbccompany.coworking.Service.SaldoClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@Controller
@RequestMapping(value = "/api/saldocliente")
public class SaldoClienteController {

    @Autowired
    private SaldoClienteService service;

    @PostMapping(value = "/novo")
    @ResponseBody
    public SaldoCliente adicionarSaldoCliente(@RequestBody SaldoCliente saldoCliente) {
        return service.salvar(saldoCliente);
    }
    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public SaldoCliente editarSaldoCliente(@PathVariable SaldoClienteId id, @RequestBody SaldoCliente saldoCliente) {
        return service.editar( id, saldoCliente);
    }
    @RequestMapping( value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Boolean deletarSaldoCliente( @PathVariable Integer id ) {
        service.deletar( id );
        return true;
    }
    @GetMapping(value = "/{id}")
    @ResponseBody
    public SaldoCliente buscarId(@PathVariable Integer id) {
        return service.saldoClienteEspecifico(id);
    }
    @GetMapping(value = "/todos")
    @ResponseBody
    public List<SaldoCliente> todosSaldosClientes(){
        return service.todosSaldoCliente();
    }
    @GetMapping(value = "/tipocontratacao/{tipoContratacao}")
    @ResponseBody
    public SaldoCliente burcarTipoContratacao(@PathVariable TipoContratacao tipoContratacao){
        return service.tipoContratoEspecifico( tipoContratacao );
    }
    @GetMapping(value = "/qtd/{qtd}")
    @ResponseBody
    public SaldoCliente burcarQtd(@PathVariable Integer qtd){
        return service.quantidadeEspecifica( qtd );
    }
    @GetMapping(value = "/vencimento/{vencimento}")
    @ResponseBody
    public SaldoCliente burcarVencimento(@PathVariable Date vencimento ){
        return service.vencimentoEspecifico( vencimento );
    }
}
