package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.ClientePacote;
import br.com.dbccompany.coworking.Service.ClientePacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "/api/clientepacote")
public class ClientePacoteController {

    @Autowired
    private ClientePacoteService service;

    @PostMapping(value = "/novo")
    @ResponseBody
    public ClientePacote adicionarClientePacote(@RequestBody ClientePacote clientePacote) {
        return service.salvar(clientePacote);
    }
    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ClientePacote editarClientePacote(@PathVariable Integer id, @RequestBody ClientePacote clientePacote) {
        return service.editar( id, clientePacote);
    }
    @RequestMapping( value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Boolean deletarClientePacote( @PathVariable Integer id ) {
        service.deletar( id );
        return true;
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public ClientePacote buscarId(@PathVariable Integer id) {
        return service.clientePacoteEspecifico(id);
    }

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<ClientePacote> todosClientesPacotes(){
        return service.todosClientesPacotes();
    }
    @GetMapping(value = "/quantidade/{qtd}")
    @ResponseBody
    public ClientePacote burcarValor(@PathVariable Integer qtd){
        return service.quantidadeEspecifica(qtd);
    }

}
