package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Cliente;
import br.com.dbccompany.coworking.Entity.TipoContato;
import br.com.dbccompany.coworking.Service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@Controller
@RequestMapping(value = "/api/cliente")
public class ClienteController {

    @Autowired
    private ClienteService service;

    @PostMapping(value = "/novo")
    @ResponseBody
    public Cliente adicionarCliente(@RequestBody Cliente cliente) {
        return service.salvar(cliente);
    }
    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Cliente editarCliente(@PathVariable Integer id, @RequestBody Cliente cliente) {
        return service.editar( id, cliente);
    }
    @RequestMapping( value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Boolean deletarCliente( @PathVariable Integer id ) {
        service.deletar( id );
        return true;
    }
    @GetMapping(value = "/{id}")
    @ResponseBody
    public Cliente buscarId(@PathVariable Integer id) {
        return service.clienteEspecifico(id);
    }
    @GetMapping(value = "/todos")
    @ResponseBody
    public List<Cliente> todosClientes(){
        return service.todosClientes();
    }

    @GetMapping(value = "/nome/{nome}")
    @ResponseBody
    public Cliente burcarNome(@PathVariable String nome){
        return service.nomeEspecifico( nome );
    }
    @GetMapping(value = "/email/{cpf}")
    @ResponseBody
    public Cliente burcarCpf(@PathVariable String cpf){
        return service.cpfEspecifico( cpf );
    }
    @GetMapping(value = "/datanascimento/{data}")
    @ResponseBody
    public Cliente burcarNascimento(@PathVariable Date data ){
        return service.dataNascimentoEspecifica( data );
    }

}
