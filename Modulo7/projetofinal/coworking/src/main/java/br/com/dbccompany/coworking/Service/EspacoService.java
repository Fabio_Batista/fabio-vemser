package br.com.dbccompany.coworking.Service;


import br.com.dbccompany.coworking.Entity.Contato;
import br.com.dbccompany.coworking.Entity.Espaco;
import br.com.dbccompany.coworking.Repository.EspacoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class EspacoService {

    @Autowired
    private EspacoRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public Espaco salvar(Espaco espaco){
        return repository.save(espaco);
    }
    @Transactional(rollbackFor = Exception.class)
    public Espaco editar(Integer id, Espaco espaco){
        espaco.setId(id);
        return repository.save(espaco);
    }
    @Transactional(rollbackFor = Exception.class)
    public void deletar(Integer id){
        repository.deleteById( id );
    }
    public List<Espaco> todosEspacos(){
        return repository.findAll();
    }
    public Espaco espacoEspecifico(Integer id){
        Optional<Espaco> Espaco = repository.findById(id);
        return Espaco.get();
    }
    public Espaco nomeEspecifico( String nome ) {
        return repository.findByNome( nome );
    }
    public List<Espaco> espacosPorNomes( String nome ) {
        return (List<Espaco>) repository.findAllByNome( nome );
    }
    public Espaco qtdPessoas( Integer qtdPessoas ) {
        return repository.findByQtdPessoas( qtdPessoas );
    }
    public Espaco valorEspecifico( Double valor ) {
        return repository.findByValor( valor );
    }

}
