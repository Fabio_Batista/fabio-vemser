package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Contato;
import br.com.dbccompany.coworking.Service.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "/api/contato")
public class ContatoController {

    @Autowired
    ContatoService service;

    @GetMapping(value = "/{id}")
    @ResponseBody
    public Contato buscarId(@PathVariable Integer id) {
        return service.contatoEspecifico(id);
    }
    @PostMapping(value = "/novo")
    @ResponseBody
    public Contato adicionarContato(@RequestBody Contato contato) {
        return service.salvar(contato);
    }
    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Contato editarContato(@PathVariable Integer id, @RequestBody Contato contato) {
        return service.editar( id, contato);
    }

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<Contato> todosContatos(){
        return service.todosContatos();
    }
    @GetMapping(value = "/valor/{valor}")
    @ResponseBody
    public Contato burcarValor(@PathVariable String valor){
        return service.valorEspecifico(valor);
    }
    @RequestMapping( value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Boolean deletarContato( @PathVariable Integer id ) {
        service.deletar( id );
        return true;
    }
}
