package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Acesso;
import br.com.dbccompany.coworking.Service.AcessoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@Controller
@RequestMapping(value = "/api/acesso")
public class AcessoController {

    @Autowired
    private AcessoService service;

    @PostMapping(value = "/novo")
    @ResponseBody
    public Acesso adicionarAcesso(@RequestBody Acesso acesso) {
        return service.salvar(acesso);
    }
    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Acesso editarAcesso(@PathVariable Integer id, @RequestBody Acesso acesso) {
        return service.editar( id, acesso);
    }
    @RequestMapping( value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Boolean deletarAcesso( @PathVariable Integer id ) {
        service.deletar( id );
        return true;
    }
    @GetMapping(value = "/{id}")
    @ResponseBody
    public Acesso buscarId(@PathVariable Integer id) {
        return service.acessoEspecifico(id);
    }
    @GetMapping(value = "/todos")
    @ResponseBody
    public List<Acesso> todosAcessos(){
        return service.todosAcessos();
    }
    @GetMapping(value = "/data/{data}")
    @ResponseBody
    public Acesso burcarData(@PathVariable Date data ){
        return service.dataEspecifica( data );
    }
}
