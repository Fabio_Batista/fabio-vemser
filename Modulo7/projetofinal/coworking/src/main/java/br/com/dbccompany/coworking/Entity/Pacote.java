package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table( name = "PACOTES")
public class Pacote {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "PACOTE_SEQ", sequenceName = "PACOTE_SEQ")
    @GeneratedValue(generator = "PACOTE_SEQ", strategy = GenerationType.SEQUENCE)

    @Column( name = "ID_PACOTE", nullable = false)
    private Integer id;

    @Column( name = "VALOR", nullable = false)
    private Double valor;

    @OneToMany(mappedBy = "pacote")
    Set<EspacoPacote>  espacosPacotes;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }
}
