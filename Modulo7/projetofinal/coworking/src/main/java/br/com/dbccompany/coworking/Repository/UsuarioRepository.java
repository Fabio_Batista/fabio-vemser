package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.Usuario;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsuarioRepository extends CrudRepository<Usuario, Integer> {
    Usuario findByNome( String nome );
    Usuario findByEmail( String email );
    Usuario findByLogin( String login );
    List<Usuario> findAll();
}
