package br.com.dbccompany.coworking.Entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.validator.constraints.br.CPF;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table( name = "CLIENTES")
public class Cliente {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "CLIENTE_SEQ", sequenceName = "CLIENTE_SEQ")
    @GeneratedValue(generator = "CLIENTE_SEQ", strategy = GenerationType.SEQUENCE)

    @Column( name = "ID_CLIENTE", nullable = false)
    private Integer id;

    @Column( name = "NOME", nullable = false, length = 50)
    private String nome;

    @CPF
    @Column( name = "CPF", nullable = false, length = 14, unique = true)
    private String cpf;

    @JsonFormat( shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy" )
    @Column( name = "DATA_NASCIMENTO", nullable = false)
    private Date dataNascimento;

    @OneToMany( mappedBy = "cliente" )
    private List<Contato> contatos = new ArrayList<>();

    @OneToMany( mappedBy = "cliente" )
    private List<Contratacao> contratacoes = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }
}
