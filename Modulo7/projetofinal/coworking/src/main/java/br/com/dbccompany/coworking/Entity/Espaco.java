package br.com.dbccompany.coworking.Entity;




import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "ESPACOS")
public class Espaco {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "ESPACO_SEQ", sequenceName = "ESPACO_SEQ")
    @GeneratedValue(generator = "ESPACO_SEQ", strategy = GenerationType.SEQUENCE)

    @Column( name = "ID_ESPACO", nullable = false)
    private Integer id;

    @Column( name = "NOME", nullable = false, length = 50, unique = true)
    private String nome;

    @Column( name = "QTD_PESSOAS", nullable = false)
    private Integer qtdPessoas;

    @Column( name = "VALOR", nullable = false)
    private Double valor;

    @OneToMany( mappedBy = "espaco")
    Set<SaldoCliente> saldoClientes;

    @OneToMany(mappedBy = "espaco")
    Set<EspacoPacote> espacosPacotes;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getQtdPessoas() {
        return qtdPessoas;
    }

    public void setQtdPessoas(Integer qtdPessoas) {
        this.qtdPessoas = qtdPessoas;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }
}
