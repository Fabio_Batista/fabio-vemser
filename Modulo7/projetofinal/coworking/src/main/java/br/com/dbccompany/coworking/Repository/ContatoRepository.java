package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.Contato;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContatoRepository extends CrudRepository<Contato, Integer> {
    Contato findByValor( String valor );
    List<Contato> findAll();
}
