package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Acesso;
import br.com.dbccompany.coworking.Repository.AcessoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class AcessoService {
    @Autowired
    private AcessoRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public Acesso salvar( Acesso acesso ) {
        return repository.save( acesso );
    }
    @Transactional ( rollbackFor = Exception.class )
    public Acesso editar( Integer id, Acesso acesso ) {
        acesso.setId(id);
        return repository.save(acesso);
    }
    @Transactional ( rollbackFor = Exception.class )
    public void deletar( Integer id ){
        repository.deleteById( id );
    }
    public Acesso acessoEspecifico( Integer id ){
        Optional<Acesso> acesso = repository.findById( id );
        return acesso.get();
    }
    public List<Acesso> todosAcessos(){
        return repository.findAll();
    }
    public Acesso dataEspecifica( Date date ) {
        return repository.findByData( date );
    }



}
