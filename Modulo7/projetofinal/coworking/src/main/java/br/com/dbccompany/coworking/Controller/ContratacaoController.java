package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Contratacao;
import br.com.dbccompany.coworking.Entity.TipoContratacao;
import br.com.dbccompany.coworking.Service.ContratacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@Controller
@RequestMapping(value = "/api/contratacao")
public class ContratacaoController {

    @Autowired
    private ContratacaoService service;

    @PostMapping(value = "/novo")
    @ResponseBody
    public Contratacao adicionarContratacao(@RequestBody Contratacao contratacao) {
        return service.salvar(contratacao);
    }
    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Contratacao editarContratacao(@PathVariable Integer id, @RequestBody Contratacao contratacao) {
        return service.editar( id, contratacao);
    }
    @RequestMapping( value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Boolean deletarContratacao( @PathVariable Integer id ) {
        service.deletar( id );
        return true;
    }
    @GetMapping(value = "/{id}")
    @ResponseBody
    public Contratacao buscarId(@PathVariable Integer id) {
        return service.contratacaoEspecifico(id);
    }
    @GetMapping(value = "/todos")
    @ResponseBody
    public List<Contratacao> todasContratacaos(){
        return service.todasContratacoes();
    }
    @GetMapping(value = "/tipocontratacao/{tipoContratacao}")
    @ResponseBody
    public Contratacao burcarTipoContratacao(@PathVariable TipoContratacao tipoContratacao){
        return service.tipoContratacaoEspecifica( tipoContratacao );
    }
    @GetMapping(value = "/quantidade/{qtd}")
    @ResponseBody
    public Contratacao burcarQtd(@PathVariable Integer qtd){
        return service.quantidadeEspecifica( qtd );
    }
    @GetMapping(value = "/desconto/{desconto}")
    @ResponseBody
    public Contratacao burcarDesconto(@PathVariable Double desconto ){
        return service.descontoEspecifico( desconto );
    }
    @GetMapping(value = "/prazo/{prazo}")
    @ResponseBody
    public Contratacao burcarPrazo(@PathVariable Integer prazo ){
        return service.prazoEspecifico( prazo );
    }
}
