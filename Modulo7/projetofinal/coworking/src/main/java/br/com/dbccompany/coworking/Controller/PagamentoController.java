package br.com.dbccompany.coworking.Controller;


import br.com.dbccompany.coworking.Entity.Pagamento;
import br.com.dbccompany.coworking.Entity.TipoPagamento;
import br.com.dbccompany.coworking.Service.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "/api/pagamento")
public class PagamentoController {

    @Autowired
    private PagamentoService service;

    @PostMapping(value = "/novo")
    @ResponseBody
    public Pagamento adicionarPagamento(@RequestBody Pagamento pagamento) {
        return service.salvar(pagamento);
    }
    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Pagamento editarPagamento(@PathVariable Integer id, @RequestBody Pagamento pagamento) {
        return service.editar( id, pagamento);
    }
    @RequestMapping( value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Boolean deletarPagamento( @PathVariable Integer id ) {
        service.deletar( id );
        return true;
    }
    @GetMapping(value = "/{id}")
    @ResponseBody
    public Pagamento buscarId(@PathVariable Integer id) {
        return service.pagamentoEspecifico(id);
    }
    @GetMapping(value = "/todos")
    @ResponseBody
    public List<Pagamento> todosPagamentos(){
        return service.todosPagamentos();
    }
    @GetMapping(value = "/tipopagamento/{tipoPagamento}")
    @ResponseBody
    public Pagamento burcarTipoPagamento(@PathVariable TipoPagamento tipoPagamento ){
        return service.tipoPagamentoEspecifico( tipoPagamento );
    }
}
