package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Espaco;
import br.com.dbccompany.coworking.Service.EspacoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "/api/espaco")
public class EspacoController {

    @Autowired
    private EspacoService service;

    @PostMapping(value = "/novo")
    @ResponseBody
    public Espaco adicionarEspaco(@RequestBody Espaco espaco) {
        return service.salvar(espaco);
    }
    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Espaco editarEspaco(@PathVariable Integer id, @RequestBody Espaco espaco) {
        return service.editar( id, espaco);
    }
    @RequestMapping( value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Boolean deletarEspaco( @PathVariable Integer id ) {
        service.deletar( id );
        return true;
    }
    @GetMapping(value = "/{id}")
    @ResponseBody
    public Espaco buscarId(@PathVariable Integer id) {
        return service.espacoEspecifico(id);
    }
    @GetMapping(value = "/todos")
    @ResponseBody
    public List<Espaco> todosEspacos(){
        return service.todosEspacos();
    }
    @GetMapping(value = "/nome/{nome}")
    @ResponseBody
    public Espaco burcarNome(@PathVariable String nome){
        return service.nomeEspecifico( nome );
    }
    @GetMapping(value = "/todos/nome/{nome}")
    @ResponseBody
    public List<Espaco> burcarTodosNome(@PathVariable String nome){
        return service.espacosPorNomes( nome );
    }
    @GetMapping(value = "/qtdpessoas/{qtd}")
    @ResponseBody
    public Espaco burcarQtdPessoas(@PathVariable Integer qtd){
        return service.qtdPessoas( qtd );
    }
    @GetMapping(value = "/valor/{valor}")
    @ResponseBody
    public Espaco burcarValor(@PathVariable Double valor ){
        return service.valorEspecifico( valor );
    }
}
