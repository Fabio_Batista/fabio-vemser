package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Usuario;
import br.com.dbccompany.coworking.Repository.UsuarioRepository;
import br.com.dbccompany.coworking.Security.Encripty;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class UsuarioService {
    @Autowired
    private UsuarioRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public Usuario salvar(Usuario usuario){
        String senha = usuario.getSenha();
        if( (StringUtils.isAlphanumeric(senha)) && senha.length() >= 6 ){
            usuario.setSenha(Encripty.encripty(senha));
            return repository.save(usuario);
        }else {
            throw new RuntimeException("Obrigátorio senha de 6 digitos alfanumericos.");
        }
    }
    @Transactional(rollbackFor = Exception.class)
    public Usuario editar(Integer id, Usuario usuario){
        usuario.setId(id);
        return repository.save(usuario);
    }
    @Transactional(rollbackFor = Exception.class)
    public void deletar(Integer id){
        repository.deleteById( id );
    }
    public List<Usuario> todosUsuarios(){
        return repository.findAll();
    }
    public Usuario usuarioEspecifico(Integer id){
        Optional<Usuario> usuario = repository.findById(id);
        return usuario.get();
    }
    public Usuario usuarioEspecificoNome( String nome ) {
        return  repository.findByNome( nome );
    }
    public Usuario usuarioEspecificoEmail( String email ) {
        return repository.findByEmail( email );
    }
    public Usuario usuarioEspecificoLogin( String login ) {
        return repository.findByLogin( login );
    }
}
