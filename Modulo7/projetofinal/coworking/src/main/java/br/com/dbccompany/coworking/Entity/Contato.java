package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;

@Entity
@Table( name = "CONTATOS")
public class Contato {
    @Id
    @SequenceGenerator( allocationSize = 1, name ="CONTATO_SEQ", sequenceName = "CONTATO_SEQ")
    @GeneratedValue(generator = "CONTATO_SEQ", strategy = GenerationType.SEQUENCE)

    @Column( name = "ID_CONTATO", nullable = false)
    private Integer id;

    @Column( name = "VALOR", nullable = false, length = 60 )
    private String valor;

    @ManyToOne( cascade = CascadeType.MERGE)
    @JoinColumn(name = "ID_TIPO_CONTATO", nullable = false)
    private TipoContato tipoContato;

    @ManyToOne( cascade = CascadeType.MERGE )
    @JoinColumn( name = "ID_CLIENTE", nullable = false )
    private Cliente cliente;

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public TipoContato getTipoContato() {
        return tipoContato;
    }

    public void setTipoContato(TipoContato tipoContato) {
        this.tipoContato = tipoContato;
    }
}
