package br.com.dbccompany.coworking.Entity;


import javax.persistence.*;

@Entity
@Table(name = "PAGAMENTOS")
public class Pagamento {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "PAGAMENTO_SEQ", sequenceName = "PAGAMENTO_SEQ")
    @GeneratedValue(generator = "PAGAMENTO_SEQ", strategy = GenerationType.SEQUENCE)

    @Column(name = "ID_PAGAMENTO", nullable = false)
    private Integer id;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "ID_CLIENTE_PACOTE")
    private ClientePacote clientePacote;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "ID_CONTRATACAO")
    private Contratacao contratacao;


    @Column(name = "ID_TIPO_PAGAMENTO")
    private TipoPagamento tipoPagamento;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ClientePacote getClientePacote() {
        return clientePacote;
    }

    public void setClientePacote(ClientePacote clientePacote) {
        this.clientePacote = clientePacote;
    }

    public Contratacao getContratacao() {
        return contratacao;
    }

    public void setContratacao(Contratacao contratacao) {
        this.contratacao = contratacao;
    }

    public TipoPagamento getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(TipoPagamento tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }
}
