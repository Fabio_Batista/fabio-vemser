package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.TipoContato;
import br.com.dbccompany.coworking.Service.TipoContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "/api/tipocontato")
public class TipoContatoController {

    @Autowired
    TipoContatoService service;

    @GetMapping(value = "/{id}")
    @ResponseBody
    public TipoContato buscarId(@PathVariable Integer id) {
        return service.tipoContatoEspecifico(id);
    }
    @PostMapping(value = "/novo")
    @ResponseBody
    public TipoContato adicionarTipoContato(@RequestBody TipoContato tipoContato) {
        return service.salvar(tipoContato);
    }
    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public TipoContato editarTipoContato(@PathVariable Integer id, @RequestBody TipoContato tipoContato) {
        return service.editar( id, tipoContato);
    }

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<TipoContato> todosTiposContatos(){
        return service.todosTiposContatos();
    }
    @GetMapping(value = "/nome/{nome}")
    @ResponseBody
    public TipoContato burcarNome(@PathVariable String nome){
        return service.nomeEspecifico(nome);
    }
    @RequestMapping( value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Boolean deletarTipoContato( @PathVariable Integer id ) {
        service.deletar( id );
        return true;
    }
}
