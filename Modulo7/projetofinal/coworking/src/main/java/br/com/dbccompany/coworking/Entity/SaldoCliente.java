package br.com.dbccompany.coworking.Entity;


import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "SALDOS_CLIENTES")
public class SaldoCliente {

    @EmbeddedId
    private SaldoClienteId id;

    @ManyToOne(cascade = CascadeType.MERGE)
    @MapsId( "ID_CLIENTE" )
    @JoinColumn( name = "ID_CLIENTE")
    private Cliente cliente;

    @ManyToOne(cascade = CascadeType.MERGE)
    @MapsId( "ID_ESPACO" )
    @JoinColumn( name = "ID_ESPACO")
    private Espaco espaco;

    @Column( name = "TIPO_CONTRATACAO", nullable = false)
    private TipoContratacao tipoContratacao;

    @Column(name = "QUANTIDADE", nullable = false)
    private Integer quantidade;

    @Column(name = "VENCIMENTO", nullable = false)
    private Date vencimento;

    public SaldoClienteId getId() {
        return id;
    }

    public void setId(SaldoClienteId id) {
        this.id = id;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Espaco getEspaco() {
        return espaco;
    }

    public void setEspaco(Espaco espaco) {
        this.espaco = espaco;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Date getVencimento() {
        return vencimento;
    }

    public void setVencimento(Date vencimento) {
        this.vencimento = vencimento;
    }
}
