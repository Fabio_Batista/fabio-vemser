package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Pacote;
import br.com.dbccompany.coworking.Service.PacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@Controller
@RequestMapping(value = "/api/pacote")
public class PacoteController {

    @Autowired
    private PacoteService service;

    @PostMapping(value = "/novo")
    @ResponseBody
    public Pacote adicionarPacote(@RequestBody Pacote pacote) {
        return service.salvar(pacote);
    }
    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Pacote editarPacote(@PathVariable Integer id, @RequestBody Pacote pacote) {
        return service.editar( id, pacote);
    }
    @RequestMapping( value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Boolean deletarPacote( @PathVariable Integer id ) {
        service.deletar( id );
        return true;
    }
    @GetMapping(value = "/{id}")
    @ResponseBody
    public Pacote buscarId(@PathVariable Integer id) {
        return service.pacoteEspecifico(id);
    }
    @GetMapping(value = "/todos")
    @ResponseBody
    public List<Pacote> todosPacotes(){
        return service.todosPacotes();
    }
    @GetMapping(value = "/valor/{valor}")
    @ResponseBody
    public Pacote burcarValor(@PathVariable Double valor ){
        return service.valorEspecifico( valor );
    }
}

