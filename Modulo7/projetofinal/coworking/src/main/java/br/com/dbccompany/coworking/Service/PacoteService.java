package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Pacote;
import br.com.dbccompany.coworking.Repository.PacoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class PacoteService {
    @Autowired
    private PacoteRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public Pacote salvar(Pacote pacote){
        return repository.save(pacote);
    }
    @Transactional(rollbackFor = Exception.class)
    public Pacote editar(Integer id, Pacote pacote){
        pacote.setId(id);
        return repository.save(pacote);
    }
    @Transactional(rollbackFor = Exception.class)
    public void deletar(Integer id){
        repository.deleteById( id );
    }
    public List<Pacote> todosPacotes(){
        return repository.findAll();
    }
    public Pacote pacoteEspecifico(Integer id){
        Optional<Pacote> pacote = repository.findById(id);
        return pacote.get();
    }
    public Pacote valorEspecifico( Double valor ) {
        return repository.findByValor( valor );
    }

}
