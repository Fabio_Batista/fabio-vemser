package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Pacote;
import br.com.dbccompany.coworking.Entity.Pagamento;
import br.com.dbccompany.coworking.Entity.TipoPagamento;
import br.com.dbccompany.coworking.Repository.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public Pagamento salvar(Pagamento pagamento) {
        return repository.save(pagamento);
    }

    @Transactional(rollbackFor = Exception.class)
    public Pagamento editar(Integer id, Pagamento pagamento) {
        pagamento.setId(id);
        return repository.save(pagamento);
    }

    @Transactional(rollbackFor = Exception.class)
    public void deletar(Integer id) {
        repository.deleteById(id);
    }
    public List<Pagamento> todosPagamentos(){
        return repository.findAll();
    }

    public Pagamento pagamentoEspecifico(Integer id) {
        Optional<Pagamento> pagamento = repository.findById(id);
        return pagamento.get();
    }

    public Pagamento tipoPagamentoEspecifico(TipoPagamento tipoPagamento) {
        return repository.findByTipoPagamento(tipoPagamento);
    }
}
