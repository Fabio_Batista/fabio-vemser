package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Cliente;
import br.com.dbccompany.coworking.Entity.Usuario;
import br.com.dbccompany.coworking.Service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@Controller
@RequestMapping(value = "/api/usuario")
public class UsuarioController {

    @Autowired
    UsuarioService service;

    @GetMapping(value = "/{id}")
    @ResponseBody
    public Usuario buscarId(@PathVariable Integer id) {
        return service.usuarioEspecifico(id);
    }
    @PostMapping(value = "/novo")
    @ResponseBody
    public Usuario adicionarUsuario(@RequestBody Usuario usuario) {
        return service.salvar(usuario);
    }
    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Usuario editarUsuario(@PathVariable Integer id, @RequestBody Usuario usuario) {
        return service.editar( id, usuario);
    }

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<Usuario> todosUsuarios(){
        return service.todosUsuarios();
    }
    @GetMapping(value = "/nome/{nome}")
    @ResponseBody
    public Usuario burcarNome(@PathVariable String nome){
        return service.usuarioEspecificoNome( nome );
    }
    @GetMapping(value = "/email/{email}")
    @ResponseBody
    public Usuario burcarEmail(@PathVariable String email){
        return service.usuarioEspecificoEmail( email );
    }
    @GetMapping(value = "/login/{login}")
    @ResponseBody
    public Usuario burcarLogin(@PathVariable String login ){
        return service.usuarioEspecificoLogin( login );
    }

    @RequestMapping( value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Boolean deletarUsuario( @PathVariable Integer id ) {
        service.deletar( id );
        return true;
    }
}
