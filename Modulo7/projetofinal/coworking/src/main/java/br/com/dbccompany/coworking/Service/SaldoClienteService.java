package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.SaldoCliente;
import br.com.dbccompany.coworking.Entity.SaldoClienteId;
import br.com.dbccompany.coworking.Entity.TipoContato;
import br.com.dbccompany.coworking.Entity.TipoContratacao;
import br.com.dbccompany.coworking.Repository.SaldoClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class SaldoClienteService {

    @Autowired
    private SaldoClienteRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public SaldoCliente salvar(SaldoCliente saldoCliente) {
        return repository.save(saldoCliente);
    }

    @Transactional(rollbackFor = Exception.class)
    public SaldoCliente editar(SaldoClienteId id, SaldoCliente saldoCliente) {
        saldoCliente.setId(id);
        return repository.save(saldoCliente);
    }

    @Transactional(rollbackFor = Exception.class)
    public void deletar(Integer id) {
        repository.deleteById(id);
    }

    public SaldoCliente saldoClienteEspecifico(Integer id) {
        Optional<SaldoCliente> saldoCliente = repository.findById(id);
        return saldoCliente.get();
    }

    public List<SaldoCliente> todosSaldoCliente(){
        return repository.findAll();
    }

    public SaldoCliente tipoContratoEspecifico(TipoContratacao tipoContratacao) {
        return repository.findByTipoContratacao(tipoContratacao);
    }

    public SaldoCliente quantidadeEspecifica(Integer quantidade) {
        return repository.findByQuantidade(quantidade);
    }

    public SaldoCliente vencimentoEspecifico(Date vencimento) {
        return repository.findByVencimento(vencimento);
    }


}