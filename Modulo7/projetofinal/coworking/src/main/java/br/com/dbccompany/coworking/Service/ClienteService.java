package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Cliente;
import br.com.dbccompany.coworking.Entity.Espaco;
import br.com.dbccompany.coworking.Entity.TipoContato;
import br.com.dbccompany.coworking.Repository.ClienteRepository;
import br.com.dbccompany.coworking.Repository.TipoContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository repository;

    @Autowired
    private TipoContatoRepository repositoryTipoConato;

    @Transactional(rollbackFor = Exception.class)
    public Cliente salvar( Cliente cliente ){
        TipoContato fone = new TipoContato();
        TipoContato email = new TipoContato();
        fone.setNome("telefone");
        email.setNome("email");
        return repository.save(cliente);
    }
    @Transactional(rollbackFor = Exception.class)
    public Cliente editar( Integer id, Cliente cliente){
        cliente.setId(id);
        return repository.save(cliente);
    }
    @Transactional(rollbackFor = Exception.class)
    public void deletar(Integer id){
        repository.deleteById(id);
    }
    public List<Cliente> todosClientes(){
        return repository.findAll();
    }
    public Cliente clienteEspecifico(Integer id){
        Optional<Cliente> cliente = repository.findById(id);
        return cliente.get();
    }
    public Cliente nomeEspecifico( String nome ){
        return  repository.findByNome( nome );
    }
    public Cliente cpfEspecifico( String cpf ){
        return  repository.findByCpf( cpf );
    }
    public Cliente dataNascimentoEspecifica( Date dataNascimento ){
        return  repository.findByDataNascimento( dataNascimento );
    }

}

