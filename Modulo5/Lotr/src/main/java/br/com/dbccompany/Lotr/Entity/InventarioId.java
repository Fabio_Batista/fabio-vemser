package br.com.dbccompany.Lotr.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;


@Embeddable
public class InventarioId {
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "FK_ID_ITEM,",
		joinColumns = { 
				@JoinColumn(name="id_inventario")},
		inverseJoinColumns = {
				@JoinColumn(name="id_item")})
	private List<ItemEntity> itens = new ArrayList<>();
		@OneToOne( cascade = CascadeType.ALL)
		@JoinColumn(name="FK_ID_PERSONAGEM")
		private PersonagemEntity personagem;
		
		public InventarioId() {
			
		}
		public InventarioId(ArrayList<ItemEntity> itens, PersonagemEntity personagem) {
			this.itens = itens;
			this.personagem = personagem;
		}
}
