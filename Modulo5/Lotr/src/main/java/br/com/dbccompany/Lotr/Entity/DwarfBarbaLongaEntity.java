package br.com.dbccompany.Lotr.Entity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="DWARF_BARBA_LONGA")
public class DwarfBarbaLongaEntity extends PersonagemEntity {
	public DwarfBarbaLongaEntity() {
		super.setTipo(Tipo.DWARF_BARBA_LONGA);
		super.setVida(110.0);
		super.setQtdDano(10.0);
		
	}
}
