package br.com.dbccompany.Cartoes.Entity;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;



@Entity
@Table( name = "CREDENCIADOR")
public class CredenciadorEntity {
	
	@Id
	@SequenceGenerator( allocationSize = 1, name = "CREDENCIADOR_SEQ", sequenceName  = "CREDENCIADOR_SEQ")
	@GeneratedValue( generator = "CREDENCIADOR_SEQ", strategy = GenerationType.SEQUENCE )
	@Column( name = "ID_CREDENCIADOR", nullable = false)
	private Integer id;
	
	@ManyToMany( mappedBy = "credenciador" )
	private List<Loja_CredenciadorEntity> loja_CredenciadorEntity = new ArrayList<>();
	
	
	@Column( name = "NOME")
	private String nome;
	
	
	public List<Loja_CredenciadorEntity> getLoja_CredenciadorEntity() {
		return loja_CredenciadorEntity ;
	}

	public void setLoja_CredenciadorEntity(List<Loja_CredenciadorEntity> loja_x_CredenciadorEntity) {
		loja_CredenciadorEntity  = loja_x_CredenciadorEntity;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}	
	
}
