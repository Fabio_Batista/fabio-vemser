package br.com.dbccompany.Cartoes;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import br.com.dbccompany.Cartoes.Entity.BandeiraEntity;
import br.com.dbccompany.Cartoes.Entity.CartaoEntity;
import br.com.dbccompany.Cartoes.Entity.ClienteEntity;
import br.com.dbccompany.Cartoes.Entity.CredenciadorEntity;
import br.com.dbccompany.Cartoes.Entity.EmissorEntity;
import br.com.dbccompany.Cartoes.Entity.HibernateUtil;
import br.com.dbccompany.Cartoes.Entity.LancamentoEntity;
import br.com.dbccompany.Cartoes.Entity.LojaEntity;
import br.com.dbccompany.Cartoes.Entity.Loja_CredenciadorEntity;

public class Main {

	public static void main(String[] args) {
		Session session = null;
		Transaction transaction = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			transaction = session.beginTransaction();
			
			LojaEntity loja = new LojaEntity();
			loja.setNome("Loja Dbc");
			session.save(loja);
			List<LojaEntity> lojas = new ArrayList<>();
			lojas.add(loja);
			
			LojaEntity loja1 = new LojaEntity();
			loja1.setNome("Magazine");
			session.save(loja1);
			lojas.add(loja1);
			
			ClienteEntity cliente = new ClienteEntity();
			cliente.setNome("Fabio");
			session.save(cliente);
			
			ClienteEntity cliente1 = new ClienteEntity();
			cliente1.setNome("Melissa");
			session.save(cliente1);	
			
			CredenciadorEntity credenciador = new CredenciadorEntity();			
			credenciador.setNome("Cred_sul");			
			session.save(credenciador);
			List<CredenciadorEntity> credenciadores = new ArrayList<>();
			credenciadores.add(credenciador);
			
			CredenciadorEntity credenciador1 = new CredenciadorEntity();			
			credenciador.setNome("Cred_Norte");			
			session.save(credenciador1);
			credenciadores.add(credenciador1);
			
			
			Loja_CredenciadorEntity loja_X_Credenciador = new Loja_CredenciadorEntity();
			loja_X_Credenciador.setLoja(lojas);
			loja_X_Credenciador.setCredenciador(credenciadores);
			loja_X_Credenciador.setTaxa(0.3);
			
			BandeiraEntity visa = new BandeiraEntity();
			visa.setNome("VISA");
			visa.setTaxa(0.6);
			session.save(visa);
			
			BandeiraEntity master = new BandeiraEntity();
			master.setNome("MASTER");
			master.setTaxa(0.8);
			session.save(master);
			
			EmissorEntity nubank = new EmissorEntity();
			nubank.setNome("Nubank");
			nubank.setTaxa(0.5);
			session.save(nubank);
			
			EmissorEntity next = new EmissorEntity();
			next.setNome("Next");
			next.setTaxa(0.8);
			session.save(next);
			
			CartaoEntity nu = new CartaoEntity();
			nu.setBandeira(visa);
			nu.setChip(1234);
			nu.setCliente(cliente);
			nu.setEmissor(nubank);
			nu.setVencimento(new Date(2019, 03, 15));
			session.save(nu);
			
			CartaoEntity inter = new CartaoEntity();
			inter.setBandeira(master);
			inter.setChip(1234);
			inter.setCliente(cliente1);
			inter.setEmissor(next);
			inter.setVencimento(new Date(2020, 02, 11));
			session.save(inter);
			
			LancamentoEntity lancamento = new LancamentoEntity();
			lancamento.setCartao(nu);
			lancamento.setDataCompra(new Date(2020, 02, 10));
			lancamento.setDescricao("Camisa");
			lancamento.setEmissor(nubank);
			lancamento.setLoja(loja);
			lancamento.setValor(5.45);
			session.save(lancamento);
			
			LancamentoEntity lancamento1 = new LancamentoEntity();
			lancamento1.setCartao(inter);
			lancamento1.setDataCompra(new Date(2020, 02, 11));
			lancamento1.setDescricao("bermuda");
			lancamento1.setEmissor(next);
			lancamento1.setLoja(loja1);
			lancamento1.setValor(15.50);
			session.save(lancamento1);
			
			session.save(loja_X_Credenciador);
			
			transaction.commit();
		} catch (Exception e) {
			if( transaction != null ) {
				transaction.rollback();
			}
			System.exit(1);
		}finally {
			System.exit(0);
		}
	}
	
}