import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DwarfBarbaLongaTest {
    @Test
    public void DwarfPerdeVida(){
        DadoFalso dado  = new DadoFalso();
        dado.simularValor(3);
        DwarfBarbaLonga dwarf = new DwarfBarbaLonga("dwarf",dado);        
        dwarf.sofrerDano();        
        assertEquals(100.0, dwarf.getVida(),1e-9);
        
        
        
    }
}
