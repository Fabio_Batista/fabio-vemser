import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
public class ElfoNoturnoTest {
    @Test
    public void ganharXpAtacando(){
        ElfoNoturno novoElfo = new ElfoNoturno("Legolas");
        Dwarf dwarf = new Dwarf("dwarf");
        
        novoElfo.atirarFlecha(dwarf);
        
        assertEquals(3, novoElfo.getExperiencia());
    }
    
    @Test
    public void perderVidaAtirandoFlecha(){
        ElfoNoturno novoElfo = new ElfoNoturno("Legolas");
        Dwarf dwarf = new Dwarf("dwarf");
        
        novoElfo.atirarFlecha(dwarf);
        novoElfo.atirarFlecha(dwarf);
        
        
        assertEquals(70.0, novoElfo.getVida(),1e-9);
        assertEquals(6, novoElfo.getExperiencia());
    }
}
