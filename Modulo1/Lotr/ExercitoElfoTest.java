import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ExercitoElfoTest {
    @Test
    public void alistarElfosValidos(){
        ExercitoElfo exercito = new ExercitoElfo();
        exercito.alistar(new ElfoNoturno("Elfo"));
        exercito.alistar(new ElfoVerde("Elfo"));
        
        assertEquals(2, exercito.getElfos().size());
    }
    
    @Test
    public void alistarElfosinvalidos(){
        ExercitoElfo exercito = new ExercitoElfo();
        exercito.alistar(new ElfoNoturno("Elfo"));
        exercito.alistar(new ElfoDaLuz("Elfo"));
        exercito.alistar(new Elfo("Elfo"));
        
        assertEquals(1, exercito.getElfos().size());
    }
    
    @Test
    public void listarElfosRecemCriados(){
        ExercitoElfo exercito = new ExercitoElfo();
        //Status recemCriado = Status.RECEM_CRIADO;
        exercito.alistar(new ElfoNoturno("Elfo"));
        exercito.alistar(new ElfoVerde("Elfo"));
        exercito.getElfos().get(0).atirarFlecha(new Dwarf("dwarf"));
        
        assertEquals(1, exercito.listaStatus(Status.RECEM_CRIADO).size());
    }
}
