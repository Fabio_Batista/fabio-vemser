

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DwarfTest
{
    @Test
    public void dwarfNasceCom110Vida(){
        Dwarf dwarf = new Dwarf("Mulungrid");
        assertEquals(110.0, dwarf.getVida(), 1e-9);
    }
    
    @Test
    public void dwarPerde10DeVida(){
        Dwarf dwarf = new Dwarf("Mulungrid");
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        
        assertEquals(90.0, dwarf.getVida(), 1e-9);
    } 
    
    @Test
    public void dwarPerdeTodaDeVida(){
        Dwarf dwarf = new Dwarf("Mulungrid");
        for( int i = 0; i < 12; i++){
            dwarf.sofrerDano();
        }
        assertEquals(0.0, dwarf.getVida(), 1e-9);
    }  
    
    @Test
    public void dwarfNasceComStaus(){
        Dwarf dwarf = new Dwarf("Gimili");        
        assertEquals(Status.RECEM_CRIADO, dwarf.getStatus() );
    }
    
    @Test
    public void dwarfSofreDanoStatus(){
        Dwarf dwarf = new Dwarf("Gimili");
        dwarf.sofrerDano();
        assertEquals(Status.SOFREU_DANO, dwarf.getStatus());
    }
    
    @Test
    public void trocaStatusParaMorto(){
        Dwarf dwarf = new Dwarf("Mulungrid");
        for( int i = 0; i < 12; i++){
            dwarf.sofrerDano();
        }
        assertEquals(Status.MORTO, dwarf.getStatus());
        //assertEquals(20.0, dwarf.getVida(), 1e-9);
    }
    
    
    @Test
    public void dwarfEquipaEscudoETomaMetadeDano(){
        Dwarf dwarf = new Dwarf("Gimli");
        dwarf.equiparEscudo();
        dwarf.sofrerDano();
        assertEquals(105.0, dwarf.getVida(), 1e-9);
    }
    
    @Test
    public void dwarfNaoEquipaEscudoETomaDanoIntegral(){
        Dwarf dwarf = new Dwarf("Gimli");
        dwarf.sofrerDano();
        assertEquals(100.0, dwarf.getVida(), 1e-9);
    }
}
