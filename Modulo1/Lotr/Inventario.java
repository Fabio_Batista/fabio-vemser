import java.util.*;
public class Inventario {
    private ArrayList<Item> itens;
    private int quantidade;
       
    
    public Inventario(int quantidade){        
        itens = new ArrayList<Item>(quantidade);              
    }
    public Inventario(){        
        itens = new ArrayList<Item>();              
    }
    public ArrayList<Item> getItens(){
        return this.itens;
    }
    public void adicionar(Item item){
        this.itens.add(item);
    }
    public Item obter(int posicao){
        if( posicao >= this.itens.size() ) {
            return null;
        }
        return this.itens.get(posicao);
    }
    public void remover(Item item){
        this.itens.remove(item);
    }
    public String getDescricoesItens(){
        StringBuilder descricoes = new StringBuilder();        
        for(int i = 0; i < this.itens.size(); i++){
            Item item = this.itens.get(i);
            
            descricoes.append(item.getDescricao());
            descricoes.append(",");
            
            
        }
        return (descricoes.length() > 0 ? descricoes.substring(0, (descricoes.length() -1)) : descricoes.toString());
    }
    
    public Item getItemComMaiorQuantidade(){
        
        int indice = 0, maiorQuantidade = 0;
        
        for(int i = 0; i < this.itens.size(); i++){
            Item item = this.itens.get(i);
            
            if (item.getQuantidade() > maiorQuantidade ){
                maiorQuantidade = item.getQuantidade();
                indice = i;                
            }
            
        }        
        return this.itens.size() > 0 ? this.itens.get(indice) : null;
    }
    
    public Item buscar(String descricao){
        int posicao = 0;
        String itemDescricao = descricao;
        /*for(int i = 0; i < this.itens.size(); i++){
            if(itemDescricao.equals(this.itens.get(i).getDescricao())){
                posicao = i;
                break;  
        }*/
        for ( Item itemAtual : this.itens ) {
            boolean encontrei = itemAtual.getDescricao().equals(descricao);
            if ( encontrei ) {
                return itemAtual;
            }
        }
        return null;
    }
    public ArrayList<Item> inverter(){
       ArrayList<Item> itensInvertidos = new ArrayList<>( this.itens.size() ); 
       for(int i = this.itens.size() - 1; i >= 0; i--){
           itensInvertidos.add(this.itens.get(i));
        }
        return itensInvertidos;
    }
    public void ordernarItens(){
        this.ordernarItens(TipoOrdenacao.ASC);
        
    }
    
    public void ordernarItens(TipoOrdenacao ordenacao){
        for ( int i = 0; i < this.itens.size(); i++){
            for (int j = 0; j < this.itens.size() - 1; j++){
                Item atual = this.itens.get(j);
                Item proximo = this.itens.get( j + 1);
                boolean deveTrocar = ordenacao == TipoOrdenacao.ASC ? 
                atual.getQuantidade() > proximo.getQuantidade():
                atual.getQuantidade() < proximo.getQuantidade();
                if ( deveTrocar ){
                    Item itemTrocado = atual;
                    this.itens.set( j, proximo);
                    this.itens.set( j + 1, itemTrocado );
                }
            }
        }
        
    }
    
    public Inventario unir(Inventario inventario){
        Inventario inventarioUnido = new Inventario();        
        for  ( Item item : this.getItens() ){
            inventarioUnido.adicionar(item);
        }
        for  ( Item item : inventario.getItens() ){
            inventarioUnido.adicionar(item);
        }
        return inventarioUnido;
    }
    public Inventario diferenciar(Inventario inventario){
        Inventario inventarioDiferente = new Inventario();
        inventarioDiferente = this;
        for ( Item item : inventario.getItens() ){
            if( inventarioDiferente.getItens().contains(item) ){
                inventarioDiferente.remover(item);
            }
        }
        return inventarioDiferente;
    }
    public Inventario cruzar( Inventario inventario ){
        Inventario inventarioCruzado = new Inventario();
        for(Item item : this.getItens() ){
            for(Item itens : inventario.getItens() ){
                if( item.getDescricao().equals(itens.getDescricao()) ) {
                    inventarioCruzado.adicionar(item);
                    inventarioCruzado.buscar(item.getDescricao()).setQuantidade( (item.getQuantidade() + itens.getQuantidade()) );
                    break;
                }                
            }
        }
        return inventarioCruzado;
    }
    
   }
   
   
