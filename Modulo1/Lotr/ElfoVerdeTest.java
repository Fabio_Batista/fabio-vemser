import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoVerdeTest {
    @Test
    public void ganhar2XpPorFlecha(){
        ElfoVerde novoElfo = new ElfoVerde("Legolas");
        Dwarf dwarf = new Dwarf("dwarf");
        
        novoElfo.atirarFlecha(dwarf);
        
        assertEquals(2, novoElfo.getExperiencia());
    }
    
    @Test
    public void ganharItemElfoVerde(){
        ElfoVerde novoElfo = new ElfoVerde("Legolas");
        Dwarf dwarf = new Dwarf("dwarf");
        Item espada = new Item(1,"Espada de aço valiriano");
        
        novoElfo.ganharItem(espada);
        
        assertEquals("Espada de aço valiriano", novoElfo.inventario.buscar("Espada de aço valiriano").getDescricao());
    }
    @Test
    public void perderItemElfoVerde(){
        ElfoVerde novoElfo = new ElfoVerde("Legolas");
        Dwarf dwarf = new Dwarf("dwarf");
        Item espada = new Item(1,"Espada de aço valiriano");
        Item arco = new Item(1,"Arco de Vidro");
        
        novoElfo.ganharItem(espada);
        novoElfo.ganharItem(arco);
        novoElfo.perderItem(novoElfo.inventario.buscar("Flecha"));
        novoElfo.perderItem(novoElfo.inventario.buscar("Arco de Vidro"));
        
        assertEquals(3, novoElfo.inventario.getItens().size());
    }
    
    @Test
    public void ganharItemDescricaoInvalidaElfoVerde(){
        ElfoVerde novoElfo = new ElfoVerde("Legolas");
        Dwarf dwarf = new Dwarf("dwarf");
        Item espada = new Item(1,"Espada de aço valiriano");
        Item arco = new Item(1,"Arco de madeira");
        
        novoElfo.ganharItem(espada);
        novoElfo.ganharItem(arco);
        novoElfo.perderItem(novoElfo.inventario.buscar("Flecha"));
        //novoElfo.perderItem(novoElfo.inventario.buscar("Arco de madeira"));
        
        assertEquals(3, novoElfo.inventario.getItens().size());
        assertEquals(new Item(3,"Flecha"), novoElfo.inventario.obter(0));
    }
}
