import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
public class ElfoDaLuzTest {
    @Test
    public void atacarComEspada(){
        ElfoDaLuz feanor = new ElfoDaLuz("Fëanor");
        
        feanor.atacarComEspada(new Dwarf("Farlum"));
        
        assertEquals(79.0, feanor.getVida(), 1e-9);
        feanor.atacarComEspada(new Dwarf("Farlum"));
        assertEquals(89.0, feanor.getVida(), 1e-9);
        assertEquals(2, feanor.getExperiencia());
        feanor.perderItem(feanor.inventario.obter(2));
        assertEquals(3, feanor.inventario.getItens().size());
        feanor.atacarComEspada(new Dwarf("Farlum"));
        assertEquals(68.0, feanor.getVida(), 1e-9);
        
    }
}
