import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class InventarioTest {
  @Test
  public void criarInventarioSemQuantidadeInformada(){
      Inventario inventario = new Inventario();
      assertEquals(0,inventario.getItens().size());
  }
  
  @Test
  public void adicionarUmItem(){
      Inventario inventario = new Inventario();
      Item espada = new Item(1,"Espada");
      inventario.adicionar(espada);
      assertEquals(espada, inventario.getItens().get(0));
      assertEquals(1, inventario.obter(0).getQuantidade());
  }
  @Test 
  public void verificarQntItens(){
      Inventario inventario = new Inventario();
      Item cajado = new Item(2,"Cajado");
      Item anel = new Item(1,"Anel");
      
      inventario.adicionar(cajado);
      inventario.adicionar(anel);
      
      assertEquals(2, inventario.getItens().size());
   }
  @Test
  public void adicionarDoisItem(){
      Inventario inventario = new Inventario();
      Item espada = new Item(1,"Espada");
      Item escudo = new Item(1,"Escudo");
      inventario.adicionar(espada);
      inventario.adicionar(escudo);
      assertEquals(espada, inventario.obter(0));
      assertEquals(escudo, inventario.obter(1));
  }
  
  @Test
  public void obterItem(){
      Inventario inventario = new Inventario();
      Item espada = new Item(1,"Espada");     
      inventario.adicionar(espada);     
      assertEquals(espada, inventario.obter(0));
      
  }
  
  @Test
  public void removerItem(){
      Inventario inventario = new Inventario();
      Item espada = new Item(1,"Espada");
      Item escudo = new Item(1,"Escudo");
      inventario.adicionar(espada);
      inventario.adicionar(escudo);
      inventario.remover(espada);
      assertEquals(1, inventario.getItens().size());  
      inventario.remover(escudo);      
      assertEquals(0,inventario.getItens().size());      
  }
  
  @Test
  public void getDescricoesVariosItens(){
      Inventario inventario = new Inventario();
      Item espada = new Item(1,"Espada");
      Item escudo = new Item(1,"Escudo");
      Item lanca = new Item(1,"Lança");
      inventario.adicionar(espada);
      inventario.adicionar(escudo);
      inventario.adicionar(lanca);
      
      assertEquals("Espada,Escudo,Lança", inventario.getDescricoesItens());
      
  }
  
  @Test
  public void getDescricaoNenumItem(){
      Inventario inventario = new Inventario(11);
      assertEquals("", inventario.getDescricoesItens());
      
  }
  
  @Test
  public void buscarItemDescricao(){
    Inventario inventario = new Inventario();
      Item espada = new Item(1,"Espada");
      Item escudo = new Item(1,"Escudo");
      inventario.adicionar(espada);
      inventario.adicionar(escudo);     
      
      assertEquals(espada, inventario.buscar("Espada"));
      assertEquals(escudo, inventario.buscar("Escudo"));
  
  }
  
  @Test
  public void buscarItemMesmaDescricao(){
    Inventario inventario = new Inventario();
      Item escudo = new Item(1,"Escudo");
      Item escudo1 = new Item(1,"Escudo");
      inventario.adicionar(escudo);
      inventario.adicionar(escudo1);     
      
      assertEquals(escudo, inventario.buscar("Escudo"));
      
  
  }
  
  @Test
  public void inverterListVazio(){
    Inventario inventario = new Inventario();
      assertTrue(inventario.inverter().isEmpty());
      
  
  }
  
  @Test
  public void inverterListaUmItem(){
    Inventario inventario = new Inventario();
      Item espada = new Item(1,"Espada");
      inventario.adicionar(espada);
      assertEquals(espada, inventario.inverter().get(0));
      
  
  }
  
  @Test
  public void inverterDoisItens(){
    Inventario inventario = new Inventario();
      Item espada = new Item(1,"Espada"); 
      Item escudo = new Item(1,"Escudo");
      inventario.adicionar(espada);
      inventario.adicionar(escudo);
      assertEquals(escudo, inventario.inverter().get(0));
      assertEquals(espada, inventario.inverter().get(1));
      assertEquals(2, inventario.inverter().size());
  }
  
  @Test 
  public void unirInventario(){
      Dwarf dwarf = new Dwarf("dwarf");
      Elfo elfo = new Elfo("elfo");
      Inventario inventario = new Inventario();
      assertEquals(0, inventario.getItens().size());
      inventario = inventario.unir(elfo.getInventario());
      assertEquals(2,inventario.getItens().size());
      inventario = inventario.unir(dwarf.getInventario());
      assertEquals(3,inventario.getItens().size());     
  }
  @Test
  public void diferenciarInventario(){
      Elfo elfo = new Elfo("elfo");
      ElfoDaLuz elfoLuz = new ElfoDaLuz("ElfoDaLuz");
      assertEquals(1,elfoLuz.getInventario().diferenciar(elfo.getInventario()).getItens().size());
  }
  @Test
  public void diferenciarInventarioTodosItensIguais(){
      Elfo elfo = new Elfo("elfo");
      ElfoDaLuz elfoLuz = new ElfoDaLuz("ElfoDaLuz");
      elfo.getInventario().adicionar(elfoLuz.getInventario().buscar("Espada de galvorn"));
      assertEquals(0,elfoLuz.getInventario().diferenciar(elfo.getInventario()).getItens().size());
  }
  @Test
  public void diferenciarInventarioItensIguaisQuantidadesDiferentes(){
      Elfo elfo = new Elfo("elfo");
      ElfoDaLuz elfoLuz = new ElfoDaLuz("ElfoDaLuz");
      elfo.getInventario().adicionar(elfoLuz.getInventario().buscar("Espada de galvorn"));
      elfo.getInventario().obter(0).setQuantidade(5);
      assertEquals(1,elfoLuz.getInventario().diferenciar(elfo.getInventario()).getItens().size());
  }
  
  @Test
  public void cruzarMesmosItens(){
      Elfo elfo = new Elfo("elfo");
      Elfo outroElfo = new Elfo("outroElfo");
      Inventario inventario = new Inventario();
      inventario = elfo.getInventario().cruzar(outroElfo.getInventario());
      assertEquals(6, inventario.obter(0).getQuantidade());
      assertEquals(2, inventario.obter(1).getQuantidade());
      assertEquals(2, inventario.getItens().size());
  }
  @Test
  public void cruzarItensitensIguais(){
      Elfo elfo = new Elfo("elfo");
      Elfo outroElfo = new Elfo("outroElfo");
      Inventario inventario = new Inventario();
      inventario = elfo.getInventario().cruzar(outroElfo.getInventario());
      assertEquals(6, inventario.obter(0).getQuantidade());
      assertEquals(2, inventario.obter(1).getQuantidade());
      assertEquals(2, inventario.getItens().size());
  }
  @Test
  public void cruzarItensitensDiferentes(){
      Elfo elfo = new Elfo("elfo");
      Dwarf dwarf = new Dwarf("dwarf");
      Inventario inventario = new Inventario();
      inventario = dwarf.getInventario().cruzar( elfo.getInventario() );
      assertEquals(0, inventario.getItens().size());
  }
}
