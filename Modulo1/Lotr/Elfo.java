/*Classe elfo*/ 
public class Elfo extends Personagem{    
    private int indiceFlecha;
    private static int qtdElfos;
    
    
    {
        indiceFlecha = 0;
    }
    
 
    //Elfo.getInventario().adicionarItem(machado);
    
    public Elfo( String nome){
        super(nome);
        this.vida = 100.0;
        this.qtdExperienciaPorAtaque = 1;
        this.inventario.adicionar( new Item(3, "Flecha"));
        this.inventario.adicionar( new Item(1, "Arco"));
        Elfo.qtdElfos++;
    }
    
    protected void finalize()throws Throwable {
        Elfo.qtdElfos--;
    }
    public static int getQtdElfos(){
        return Elfo.qtdElfos;
    }
    public Item getFlecha(){
        return this.inventario.obter(indiceFlecha);
    }
    
    public int getQtdFlecha(){
        return this.getFlecha().getQuantidade();
    }
    
    public Boolean podeAtirarFlecha(){
        return this.getQtdFlecha() > 0;
    }
    
    public void atirarFlecha(Dwarf dwarf){
        
        if(podeAtirarFlecha()){
            this.getFlecha().setQuantidade( this.getQtdFlecha() - 1 );
            //dwarf.setQntVida(dwarf.getQntVida() -10 );
            //this.experiencia = experiencia + 1;
            this.aumentarXp();
            dwarf.sofrerDano();
            this.sofrerDano();
        }
    }
}       
