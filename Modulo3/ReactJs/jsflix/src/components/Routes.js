import React from 'react';
import Home from '../models/PaginaInicial';
import Mirror from '../models/Mirror';
import Info from '../models/Info';
import { BrowserRouter as Router, Route } from 'react-router-dom'




const Routes = () => (
    <Router>
        <Route exact path="/" component={ Home }/>
        <Route exact path="/mirror" component={ Mirror }/>
        <Route exact path="/info" component={ Info }/>
    </Router>
);
export default Routes