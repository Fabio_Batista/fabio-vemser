import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class MensagemFlash extends Component {

    fechar = () => {
        console.log('fechar');
        this.props.atualizarMensagem( false )
    }

    render(){
        const { deveExibirMensagem, mensagem, cor } = this.props
        return (
            <span onClick={ this.fechar } className={ `flash ${ cor } ${ deveExibirMensagem ? '' : 'invisivel' }` }>{ mensagem }</span>
        )
    }
}

// https://reactjs.org/docs/typechecking-with-proptypes.html
MensagemFlash.propTypes = {
    mensagem: PropTypes.string.isRequired,
    deveExibirMensagem: PropTypes.bool.isRequired,
    atualizarMensagem: PropTypes.func.isRequired,
    cor: PropTypes.string
}

MensagemFlash.defaultProps = {
    cor: 'verde'
}