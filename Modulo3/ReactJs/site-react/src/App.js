import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

import Home from './Componentes/home';
import Sobre from './Componentes/sobre_nos';
import Servico from './Componentes/servicos';
import Contato from './Componentes/contato';


export default class App extends Component {

  render() {
    return (
      <Router>
        <Route path="/" exact component={ Home } />
        <Route path="/sobre_nos" component={ Sobre } />
        <Route path="/servicos" component={ Servico } />
        <Route path="/contato" component={ Contato } />
      </Router>
    );
  }
}

/* const PaginaTeste = () =>
  <div>
    Pagina Teste
    <Link to="/">Home</Link>
    <Link to="/teste">Teste</Link>
  </div>  */