import React from 'react';
import {  Link } from 'react-router-dom';
import './../css/grid.css';
import './../css/reset.css';
import '../css/header.css'
import logo from '../img/logo-dbc-topo.png'


export default (props) =>
    <header className="main-header">
        <nav className="container clearfix">
            <a className="logo" href="index.html" title="Voltar à home">
                <img src={logo} alt="DBC Company" />
            </a>

            <label className="mobile-menu" for="mobile-menu">
                <span></span>
                <span></span>
                <span></span>
            </label>
            <input id="mobile-menu" type="checkbox" />

            <ul className="clearfix">
                <li>
                    <Link to="/">Home</Link>
                </li>
                <li>
                    <Link to="/sobre_nos">Sobre nós</Link>
                </li>
                <li>
                    <Link to="/servicos">serviços</Link>
                </li>
                <li>
                    <Link to="/contato">contato</Link>
                </li>
            </ul>
        </nav>
    </header>
