import React from 'react';
import '../css/grid.css'

export default ( props ) =>
    {
        return (
            <div className="row">
                {props.children}
            </div>
        );
    }