import React, { Component } from 'react';
import Header from './header';
import Footer from './footer';
import Article from './article';
import Row from './row';
import Text from './text';
import Input from './input';
import Button from './button';
import '../css/contact.css'

export default class Contato extends Component {
    render(){
        return(
            <React.Fragment>
                <Header/>
                    <Article coluna="container work">
                        <Row>
                            <Article coluna="col col-6">
                                <Text/>
                                <form class="clearfix">
                                    <Input classe="field" tipo="text" esp_reservado="Nome" id="campoNome" />
                                    <div id="msg-erro-nome"></div>
                                    <Input classe="field" tipo="text" esp_reservado="E-mail" id="campoEmail" />
                                    <div id="msg-erro-email"></div>
                                    <Input classe="field" tipo="text" esp_reservado="Assunto" id="campoAssunto" />
                                    <div id="msg-erro-assunto"></div>
                                    <textarea className="field" placeholder="Mensagem" id="campoMensagem" ></textarea>
                                    <div id="msg-erro-mensagem"></div>
                                    <Button classe="button button-green button-right" texto="Enviar"/>
                                </form>
                            </Article>                            
                        </Row>
                        <Article coluna="map-container">
                            <iframe class="map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3454.7200844760855!2d-51.17087028474702!3d-30.01619288189262!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x951977775fc4c071%3A0x6de693cbd6b0b5e5!2sDBC%20Company!5e0!3m2!1spt-BR!2sbr!4v1576870098547!5m2!1spt-BR!2sbr" allowfullscreen=""></iframe>
                        </Article>

                    </Article>
                    <div>

                    </div>
                <Footer classe="main-footer fixed-footer-bottom"/>
            </React.Fragment>

        );
    }
}