import React, { Component } from 'react';
import Header from './header';
import Footer from './footer';
import Card from './card';
import Row from './row'
import Article from './article';
import Button from './button'
import Text from './text';
import Img from './imagem';
import '../css/grid.css'

export default class Servicos extends Component {
    render() {
        return (
            <React.Fragment>
                <Header />
                <Article coluna="container work">
                    <Row>
                        <Article coluna="col col-4">
                            <Card>
                                <Img />
                                <Text />
                            </Card>

                        </Article>
                        <Article coluna="col col-4">
                            <Card>
                                <Img />
                                <Text />
                            </Card>

                        </Article>
                        <Article coluna="col col-4">
                            <Card>
                                <Img />
                                <Text />
                            </Card>

                        </Article>
                    </Row>
                    <Row>
                        <Article coluna="col col-4">
                            <Card>
                                <Img />
                                <Text />
                            </Card>

                        </Article>
                        <Article coluna="col col-4">
                            <Card>
                                <Img />
                                <Text />
                            </Card>

                        </Article>
                        <Article coluna="col col-4">
                            <Card>
                                <Img />
                                <Text />
                            </Card>

                        </Article>
                    </Row>
                    <Row>
                        <Article coluna="container center col-11">
                            <Text />
                            <Button classe="button button-blue" texto="Saiba mais"/>
                        </Article>

                    </Row>
                </Article>
                <Footer classe="main-footer"/>
            </React.Fragment>

        );
    }


}