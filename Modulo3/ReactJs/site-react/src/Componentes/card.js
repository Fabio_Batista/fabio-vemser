import React from 'react';
import '../css/box.css';

export default (props) =>
    <article class="box">
        {props.children}
    </article>
    