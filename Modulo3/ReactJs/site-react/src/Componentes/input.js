import React from 'react';
import '../css/contact.css'

export default (props) =>
    <React.Fragment>
        <input
            className={props.classe}
            type={props.tipo}
            placeholder={props.esp_reservado}
            id={props.id}
        />
    </React.Fragment>
