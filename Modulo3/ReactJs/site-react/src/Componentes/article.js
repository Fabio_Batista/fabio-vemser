import React from 'react';
import '../css/grid.css';
import '../css/box.css';
import '../css/work.css';
import '../css/contact.css';



export default (props) =>
    <article className={props.coluna}>
        {props.children}
    </article>
