import React, {Component} from 'react';
import Header from './header';
import Footer from './footer';
import Text from './text';
import Row from './row';
import img from '../img/foto1.jpg'
import '../css/general.css';
import '../css/about-us.css'
import '../css/grid.css';


export default class Sobre_Nos extends Component {
    render (){
        return (
            <React.Fragment>
                <Header/>
                <article className="container about-us">
                    <Row>
                        <img src={img}/>
                        <Text coluna="col col-12" />
                    </Row>
                    <Row>
                        <img src={img}/>
                        <Text coluna="col col-12" />
                    </Row>
                    <Row>
                        <img src={img}/>
                        <Text coluna="col col-12" />
                    </Row>
                    <Row>
                        <img src={img}/>
                        <Text coluna="col col-12" />
                    </Row> 
                    <Row>
                        <img src={img}/>
                        <Text coluna="col col-12" />
                    </Row> 
                                     
                </article>
                <Footer classe="main-footer"/>
            </React.Fragment>
        );
    }
}