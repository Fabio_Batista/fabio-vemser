import React from 'react';
import {  Link } from 'react-router-dom';
import '../css/footer.css';
import '../css/reset.css';

export default (props) =>
    <footer className={ props.classe }>
        <div className="container">
            <nav>
                <ul>
                    <li>
                        <Link to="/">Home</Link>
                    </li>
                    <li>
                        <Link to="/sobre_nos">Sobre nós</Link>
                    </li>
                    <li>
                        <Link to="/servicos">serviços</Link>
                    </li>
                    <li>
                        <Link to="/contato">contato</Link>
                    </li>
                </ul>
            </nav>
            <p>
                &copy; Copyright DBC Company - 2019
            </p>
        </div>
    </footer>
