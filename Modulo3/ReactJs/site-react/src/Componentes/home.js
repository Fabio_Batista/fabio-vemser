import React, { Component } from 'react';
import Header from './header';
import Footer from './footer';
import Text from './text';
import Card from './card';
import Button from './button';
import Row from './row';
import Article from './article';
import Img from './imagem';
import '../css/banner.css';
import '../css/grid.css';

const estilo_img = " height: 100px; width: 100px; border-radius: 60%;"


export default class App extends Component {
    
    render() {
        return (
            <React.Fragment>
                <Header />
                <section class="main-banner">
                    <article>
                        <Text/>
                        <Button classe="button button-blue" texto="Saiba mais"/>
                    </article>
                </section>
                <section className="container">
                    <Row>
                        <Article coluna="col col-12 col-md-7" >
                            <Text/>
                        </Article>
                        <Article coluna="col col-12 col-md-5" >
                            <Text/>
                        </Article>
                        
                    </Row>
                    <Row>
                        <Article coluna="col col-12 col-md-6 col-lg-3">
                            <Card>
                                <Img/>
                                <Text/>
                                <Button classe="button button-blue" texto="Saiba mais"/>
                            </Card>
                        </Article>
                        <Article coluna="col col-12 col-md-6 col-lg-3">
                            <Card>
                            <Img/>
                                <Text/>
                                <Button classe="button button-blue" texto="Saiba mais"/>
                            </Card>
                        </Article>
                        <Article coluna="col col-12 col-md-6 col-lg-3">
                            <Card>
                            <Img/>
                                <Text/>
                                <Button classe="button button-blue" texto="Saiba mais"/>
                            </Card>
                        </Article>
                        <Article coluna="col col-12 col-md-6 col-lg-3">
                            <Card>
                            <Img/>
                                <Text/>
                                <Button classe="button button-blue" texto="Saiba mais"/>
                            </Card>
                        </Article>

                        
                    </Row>
                </section>



                <Footer classe="main-footer"/>
            </React.Fragment>
        );
    }
}