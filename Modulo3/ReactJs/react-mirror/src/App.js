import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import TelaDetalheEpisodio from './components/TelaDetalheEpisodio';
import TelaListaDeEpisodios  from './components/telaListaDeEpisodios';
import listaepisodios from './models/listaEpisodios'

import Home from './Home';
import ListaAvaliacoes  from './components/ListaAvaliacoes';

export default class App extends Component {

  render() {
    return (
      <Router>
        <Route path="/" exact component={ Home } />
        <Route path="/avaliacoes" component={ ListaAvaliacoes } />
        <Route path="/episodio/:id" component={ TelaDetalheEpisodio } />
        <Route path="/listaepisodios" component={ TelaListaDeEpisodios } />
      </Router>
    );
  }
}