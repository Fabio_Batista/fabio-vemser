import Episodio from './episodio';

function _sortear(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * ( max - min )) + min;
}

export default class ListaEpisodios {
    constructor() {
        this.todos = [
            { id: 1 ,nome: 'Arkangel', duracao: 52, temporada: 4, ordemEpisodio: 2, thumbUrl: 'https://occ-0-678-559.1.nflxso.net/art/9aeb1/6f3ebb055fe062f85dc3f84871bdafaee6a9aeb1.jpg',  sinopse: 'After nearly losing her daughter, a mother invests in a new technology that allows her to keep track of her.', estreia: '29/12/2017', notaIMDB: 8.3 },
            { id: 2 ,nome: 'Be Right Back', duracao: 49, temporada: 2, ordemEpisodio: 1, thumbUrl: 'https://occ-0-678-559.1.nflxso.net/art/c4d52/ba1d86466a8785d4ba73c76cbcebe2e6384c4d52.jpg', sinopse: 'After learning about a new service that lets people stay in touch with the deceased, a lonely, grieving Martha reconnects with her late lover.', estreia: '23/02/2013' , notaIMDB: 8.1 },
            { id: 3 ,nome: 'Black Museum', duracao: 69, temporada: 4, ordemEpisodio: 6, thumbUrl: 'https://occ-0-678-559.1.nflxso.net/art/8b126/81549c8f021fc16cc4b8af1586ddc3b42d88b126.jpg', sinopse: 'A woman enters the Black Museum, where the proprietor tells his stories relating to the artifacts.', estreia: '29/12/2017', notaIMDB: 8.7 },
            { id: 4 ,nome: 'Crocodile', duracao: 59, temporada: 4, ordemEpisodio: 3, thumbUrl: 'https://occ-0-678-559.1.nflxso.net/art/557ff/558fcde2fe704bf3eaa2ef3b26aad3d38e6557ff.jpg', sinopse: 'A woman interviews various people using a device that allows her to access their memories.', estreia: '29/12/2017', notaIMDB: 7.3 },
            { id: 5 ,nome: 'The Entire History of You', duracao: 49, temporada: 1, ordemEpisodio: 3, thumbUrl: 'https://occ-0-678-559.1.nflxso.net/art/400a9/57eb44a76c31274fef233131d583ab7bc97400a9.jpg', sinopse: 'In the near future, everyone has access to a memory implant that records everything they do, see and hear. You need never forget a face again - but is that always a good thing?', estreia: '18/12/2011', notaIMDB: 8.6 },
            { id: 6 ,nome: 'Fifteen Million Merits', duracao: 62, temporada: 1, ordemEpisodio: 2, thumbUrl: 'https://occ-0-678-559.1.nflxso.net/art/7f50e/422928a1b9fa31b8a6c209b5df4d0915f557f50e.jpg', sinopse: 'In a world where people`s lives consist of riding exercise bikes to gain credits, Bing tries to help a woman get on to a singing competition show.', estreia: '11/12/2011', notaIMDB: 8.1 },
            { id: 7 ,nome: 'Hang the DJ', duracao: 52, temporada: 4, ordemEpisodio: 4, thumbUrl: 'https://occ-0-678-559.1.nflxso.net/art/61a3e/02aa15ddad05aa972d53bcad460aa9d527f61a3e.jpg', sinopse: 'Paired up by a dating program that puts an expiration date on all relationships, Frank and Amy soon begin to question the system`s logic.', estreia: '29/12/2017', notaIMDB: 8.8 },
            { id: 8 ,nome: 'Hated in the Nation', duracao: 90, temporada: 3, ordemEpisodio: 6, thumbUrl: 'https://occ-0-678-559.1.nflxso.net/art/6a32d/88bf30eae1ae98b82b2e8081813e731ab0c6a32d.jpg', sinopse: 'In near-future London, police detective Karin Parke, and her tech-savvy sidekick Blue, investigate a string of mysterious deaths with a sinister link to social media.', estreia: '21/10/2016', notaIMDB: 8.5 },
            { id: 9 ,nome: 'Men Against Fire', duracao: 60, temporada: 3, ordemEpisodio: 5, thumbUrl: 'https://occ-0-678-559.1.nflxso.net/art/3940b/4c0badc82119dc19439e181f19234c074a83940b.jpg', sinopse: 'Future soldiers Stripe and Raiman must protect frightened villagers from an infestation of vicious feral mutants.', estreia: '21/10/2016', notaIMDB: 7.6 },
            { id: 10 ,nome: 'Metalhead', duracao: 41, temporada: 4, ordemEpisodio: 5, thumbUrl: 'https://occ-0-678-559.1.nflxso.net/art/ab4ae/08cf3abd7ddbef2acf82350fad6ee9ebfedab4ae.jpg', sinopse: 'In the post-apocalyptic landscape of the Scottish Moors, a woman attempts to survive the land full of "dogs."', estreia: '29/12/2017', notaIMDB: 6.7 },
            { id: 11 ,nome: 'The National Anthem', duracao: 44, temporada: 1, ordemEpisodio: 1, thumbUrl: 'https://occ-0-678-559.1.nflxso.net/art/68242/db5699eda3c612123fed50166ef9598b14068242.jpg', sinopse: 'Prime Minister Michael Callow faces a shocking dilemma when Princess Susannah, a much-loved member of the Royal Family, is kidnapped.', estreia: '07/06/2019', notaIMDB: 7.8 },
            { id: 12 ,nome: 'Nosedive', duracao: 63, temporada: 3, ordemEpisodio: 1, thumbUrl: 'https://occ-0-678-559.1.nflxso.net/art/424df/608de21c697b612e00ef851052ccba447c7424df.jpg', sinopse: 'A woman desperate to boost her social media score hits the jackpot when she`s invited to a swanky wedding, but the trip doesn`t go as planned.', estreia: '21/10/2016', notaIMDB: 8.3 },
            { id: 13 ,nome: 'Playtest', duracao: 57, temporada: 3, ordemEpisodio: 2, thumbUrl: 'https://occ-0-678-559.1.nflxso.net/art/75542/cc3e412d8e66f8f4de0476eb37a23a63b0875542.jpg', sinopse: 'An American traveler short on cash signs up to test a revolutionary new gaming system, but soon can`t tell where the hot game ends and reality begins.', estreia: '21/10/2016', notaIMDB: 8.1 },
            { id: 14 ,nome: 'San Junipero', duracao: 61, temporada: 3, ordemEpisodio: 4, thumbUrl: 'https://occ-0-678-559.1.nflxso.net/art/69c24/ecfc0c072506955c45aa259293f1a2f5c9869c24.jpg', sinopse: 'When Yorkie and Kelly visit San Junipero, a fun-loving beach town full of surf, sun and sex, their lives are changed.', estreia: '21/10/2016', notaIMDB: 8.6 },
            { id: 15 ,nome: 'Shut Up and Dance', duracao: 53, temporada: 3, ordemEpisodio: 3, thumbUrl: 'https://occ-0-678-559.1.nflxso.net/art/89271/5e8b75f4582aac66f780664a86025992b4a89271.jpg', sinopse: 'When withdrawn Kenny stumbles headlong into an online trap, he is quickly forced into an uneasy alliance with shifty Hector, both at the mercy of persons unknown.', estreia: '21/10/2016', notaIMDB: 8.5 },
            { id: 16 ,nome: 'USS Callister', duracao: 77, temporada: 4, ordemEpisodio: 1, thumbUrl: 'https://occ-0-678-559.1.nflxso.net/art/8e8ef/c08aaf2ae6c9daa52320b41bef71a26921d8e8ef.jpg', sinopse: 'Capt. Robert Daly presides over his crew with wisdom and courage. But a new recruit will soon discover nothing on this spaceship is what it seems.', estreia: '29/12/2017', notaIMDB: 8.3 },
            { id: 17 ,nome: 'The Waldo Moment', duracao: 44, temporada: 2, ordemEpisodio: 3, thumbUrl: 'https://occ-0-678-559.1.nflxso.net/art/f5694/edcafaec5cd271131b245254c3106f7cd92f5694.jpg', sinopse: 'A failed comedian who voices a popular cartoon bear named Waldo finds himself mixing in politics when TV executives want Waldo to run for office.', estreia: '25/02/2013', notaIMDB: 6.7 },
            { id: 18 ,nome: 'White Bear', duracao: 42, temporada: 2, ordemEpisodio: 2, thumbUrl: 'https://occ-0-678-559.1.nflxso.net/art/abf16/6090599058bb80cae847951d5273f00e874abf16.jpg', sinopse: 'Victoria wakes up and cannot remember anything about her life. Everyone she encounters refuses to communicate with her, and they all seem to know something she doesn`t. But what?', estreia: '18/02/2013', notaIMDB: 8.1 },
            { id: 19 ,nome: 'White Christmas', duracao: 74, temporada: 2, ordemEpisodio: 4, thumbUrl: 'https://occ-0-678-559.1.nflxso.net/art/0b465/db9e48d9e27a99396cb9202601159454f6e0b465.jpg', sinopse: 'Three interconnected tales of technology run amok during the Christmas season are told by two men at a remote outpost in a frozen wilderness.', estreia: '16/12/2014', notaIMDB: 9.2 }
        ].map( objEpisodio => new Episodio( objEpisodio.id, objEpisodio.nome, objEpisodio.duracao, objEpisodio.temporada, objEpisodio.ordemEpisodio, objEpisodio.thumbUrl, objEpisodio.sinopse, objEpisodio.estreia, objEpisodio.notaIMDB ) )
    }

    get episodiosAleatorios() {
        const indice = _sortear( 0, this.todos.length - 1 )
        return this.todos[ indice ]
    }
    get avaliados(){
        return this.todos.filter( ep => ep.nota ).sort( ( a, b ) => a.temporada - b.temporada || a.ordem - b.ordem )
    }

    /* marcarComoAssistido( episodio ) {
        const episodioParaMarcar = this.todos.find( linha => linha.nome === episodio.nome )
        episodioParaMarcar.assistido = true
    } */
}