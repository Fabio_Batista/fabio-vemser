import React, { Component } from 'react';
import lista from '../models/listaEpisodios'
export default class TelaDetalheEpisodio extends Component {
    constructor ( props ){
        super ( props )
        this.state ={
            idEpi : 0,
            listaEpisodios: lista,
            episodio: this.listaEpisodios[this.idEpi]

        }
    }
    UNSAFE_componentWillMount(){
        const idEpi = this.props.match.params.id
        this.setState( { idEpi: idEpi } )
        this.setState( { episodio: lista[idEpi] } )
    }
    render(){
        
        const { episodio } = this.state
        console.log(this.episodio)
        return (
            <React.Fragment>
                <h2>{ episodio.nome }</h2>
                <img src={ episodio.url } alt={ episodio.nome }></img><br/>
                <p>Temporada: { episodio.temporada }  Episodio: { episodio.ordemEpisodio }</p>
                <span>Duração: { episodio.duracaoEmMin }</span><br/>
                <p>Sinopse: { episodio.sinopse }</p>
                <p>Data de estreia: { episodio.estreia }</p>
                <p>Sua Nota: { episodio.nota }</p>                
                <span>Nota IMDB: { episodio.notaIMDB / 2 }</span><br/>
            </React.Fragment>
        )
    }
}