import React from 'react';
import { Link } from 'react-router-dom';
const ListaAvaliacoes = props => {
    
    const { listaEpisodios } = props.location.state
    console.log(listaEpisodios)
    return listaEpisodios.avaliados.map(ep =>
            <li key ={ep.id}>
                <Link to={{ pathname: `/episodio/${ep.id}`, state:{ listaEpisodios} } }>
                    {` ${ ep.nome } Ep - ${ ep.ordem}  | Nota - ${ ep.nota }    | Temporada - ${ ep.temporada }`}
                </Link>
            </li>
    )
}
export default ListaAvaliacoes;