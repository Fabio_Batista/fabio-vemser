const pokeApi = new PokeApi();
const pesquisar = document.querySelector( '.pesquisar' );
const sortear = document.querySelector( '.sortear' );
let inputValue = 0;
let inputIdPokemon = document.querySelector( '.inputIdPokemon' );
const idMIn = 1;
const idMax = 5;

function renderizacaoPokemon( pokemon ) {
  const dadosPokemon = document.getElementById( 'dadosPokemon' );
  const nome = dadosPokemon.querySelector( '.nome' );
  const imgPokemon = dadosPokemon.querySelector( '.thumb' );
  const idPokemon = dadosPokemon.querySelector( '.id' );
  const alturaPokemon = dadosPokemon.querySelector( '.altura' );
  const pesoPokemon = dadosPokemon.querySelector( '.peso' );
  const tiposPokemon = dadosPokemon.querySelector( '.tipos' );
  const estatisticasPokemon = dadosPokemon.querySelector( '.estatisticas' );

  nome.innerHTML = pokemon.nome;
  idPokemon.innerHTML = pokemon.id;
  alturaPokemon.innerHTML = `${ pokemon.altura }cm`;
  pesoPokemon.innerHTML = `${ pokemon.peso }Kg`;
  tiposPokemon.innerHTML = `Tipos: ${ pokemon.tipos }`
  estatisticasPokemon.innerHTML = pokemon.estatisticas;
  imgPokemon.src = pokemon.imagem;
}

async function buscar( id ) {
  const pokemonEspecifico = await pokeApi.buscar( id );
  const poke = new Pokemon( pokemonEspecifico );
  if ( id === inputValue ) {
    alert( 'Id já está sendo exibido' )
  } else if ( id > 0 && id <= 803 ) {
    renderizacaoPokemon( poke );
    inputValue = id;
  } else {
    alert( 'Digite um id válido' )
  }
}

function sortearId() {
  return Math.floor( Math.random() * ( Math.floor( idMax ) - Math.ceil( idMIn ) + 1 ) ) + idMIn;
}

async function buscarTodos() {
  const idPokemonSorteado = sortearId();
  inputValue = idPokemonSorteado;
  if ( localStorage.getItem( JSON.stringify( idPokemonSorteado ) ) === null ) {
    window.localStorage.setItem( JSON.stringify( idPokemonSorteado ),
      JSON.stringify( idPokemonSorteado ) )
    const pokemonSorteado = await pokeApi.buscar( idPokemonSorteado );
    const poke = new Pokemon( pokemonSorteado );
    renderizacaoPokemon( poke );
  } else {
    alert( `Id: ${ idPokemonSorteado } já foi sorteado` )
  }
}
pesquisar.addEventListener( 'click', () => {
  inputIdPokemon = document.querySelector( '.inputIdPokemon' );
  const idPokemonPesquisado = parseInt( ( inputIdPokemon.value ), 10 );
  if ( !isNaN( idPokemonPesquisado ) ) {
    buscar( idPokemonPesquisado );
  } else {
    alert( 'Digite um valor válido' )
  }
} );

sortear.addEventListener( 'click', () => {
  buscarTodos();
  inputIdPokemon.value = '';
} )
