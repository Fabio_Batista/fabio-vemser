import React, { Component } from 'react';
import * as axios from 'axios';
import { Link } from 'react-router-dom';
import Header from '../components/header';
import Card from '../components/card';

export default class clientes extends Component {
    constructor(props) {
        super(props)
        this.state = {
            clientes: [],
            clientesFilter: []
        }
    }

    UNSAFE_componentWillMount() {
        axios.get('http://localhost:1337/clientes', {
            headers: {
                'Authorization': 'banco-vemser-api-fake'
            }
        }).then(res => {
            this.setState({
                clientes: res.data.clientes
            })
            this.setState({
                clientesFilter: res.data.clientes
            })
        })
    }
    filtrarCliente(nome) {
        const { clientes } = this.state
        const cliente = clientes.filter(cliente => cliente.nome.includes(nome.target.value))
        this.setState({
            clientesFilter: cliente
        });
    }

    render() {
        const { clientesFilter } = this.state
        return (
            <div className="container">
                <Header />
                <input placeholder="Informe o nome do cliente" onChange={this.filtrarCliente.bind(this)} />
                {clientesFilter.map(cliente =>
                    <Card key={cliente.id} >
                        <Link to={{ pathname: `/cliente/${cliente.id}`, state: { cliente: cliente } }} >
                            <h3>Nome de cliente: {cliente.nome}</h3>
                            <h3>Cpf: {cliente.cpf}</h3>                            
                        </Link>
                    </Card>
                )}                
            </div>

        )

    }
}