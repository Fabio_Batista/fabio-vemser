import React, { Component } from 'react';
import * as axios from 'axios';
import { Link } from 'react-router-dom';
import Card from '../components/card';
import Header from '../components/header';
import '../App.css'

export default class Agencias extends Component {
    constructor(props) {
        super(props)
        this.state = {
            agencias: [],
            agenciasFilter: []
        }
    }

    UNSAFE_componentWillMount() {
        axios.get('http://localhost:1337/agencias', {
            headers: {
                'Authorization': 'banco-vemser-api-fake'
            }
        }).then(res => {
            this.setState({
                agencias: res.data.agencias
            })
            this.setState({
                agenciasFilter: res.data.agencias
            })
        })
    }
    filtrarAgencia(nome) {
        const { agencias } = this.state
        const agencia = agencias.filter(agencia => agencia.nome.includes(nome.target.value.toLowerCase()))
        this.setState({
            agenciasFilter: agencia
        });
    }
    isDigital(evt) {
        const id = parseInt(evt.target.id)
        const check = evt.target.checked
        const { agencias } = this.state
        let agenciasDigitais = agencias
        if (check) {
            agenciasDigitais.map(agencia => {
                if (agencia.id === id) {
                    agencia.isDigital = true
                }
            })
            this.setState({ agenciasFilter: agenciasDigitais })

        } else {
            agenciasDigitais.map(agencia => {
                if (agencia.id === id) {
                    agencia.isDigital = false
                }
            })
            this.setState({ agenciasFilter: agenciasDigitais })

        }
    }
    filtrarAgenciaDigital() {
        const { agenciasFilter } = this.state
        const agencias = agenciasFilter.filter(agencia => agencia.isDigital === true)
        this.setState({ agenciasFilter: agencias })
    }

    render() {
        const { agenciasFilter } = this.state
        return (
            <div className="container">
                <Header />
                <input placeholder="Informe o nome da agência" onChange={this.filtrarAgencia.bind(this)} />
                <button className="filterDigital" onClick={this.filtrarAgenciaDigital.bind(this)} >Agencia digital</button>
                { agenciasFilter.map(agencia =>
                    <Card key={agencia.id}>
                        <React.Fragment >
                            <Link to={{ pathname: `/agencia/${agencia.id}`, state: { agencia: agencia } }}>
                                <h2>Agencia: {agencia.codigo} </h2>
                                <h3>Nome de Agencia: {agencia.nome}</h3>

                            </Link>
                            <input classename="input-agencia" type="checkbox" id={agencia.id} name="digital" onClick={this.isDigital.bind(this)} />Digital
                        </React.Fragment>
                    </Card>
                )}
            </div>
        )
    }
}