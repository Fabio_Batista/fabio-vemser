import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import Header from '../components/header';
import Card from '../components/card';


export default class ContasClientes extends Component {
    constructor(props) {
        super(props)
        this.state = {
            ContasClientes: [],
            contasClientesFilter: []
        }
    }

    UNSAFE_componentWillMount() {
        axios.get('http://localhost:1337/conta/clientes', {
            headers: {
                'Authorization': 'banco-vemser-api-fake'
            }
        }).then(res => {
            this.setState({
                ContasClientes: res.data.cliente_x_conta
            })
            console.log(ContasClientes);
            
            this.setState({
                contasClientesFilter: res.data.cliente_x_conta
            })
        })
    }
    filtrarContaClientes(nome) {
        const { ContasClientes } = this.state
        const contas = ContasClientes.filter(conta => conta.tipo.nome.includes(nome.target.value))
        this.setState({
            contasClientesFilter: contas
        });
    }

    render() {
        const { contasClientesFilter } = this.state
        return (
            <div className="container">
                <Header />
                <input placeholder="Informe a conta do cliente" onChange={this.filtrarContaClientes.bind(this)} />
                {contasClientesFilter.map(contas =>
                    <Card key={contas.id}>
                        <Link to={{ pathname: `/conta/cliente/${contas.id}`, state: { contas: contas } }} >
                            
                            <h4>Nome: {contas.cliente.nome}</h4>
                            <h4>Cpf: {contas.cliente.cpf}</h4>
                            <h2>Conta: {contas.tipo.nome}</h2>
                        </Link>
                    </Card>

                )}
            </div>
        )
    }
}