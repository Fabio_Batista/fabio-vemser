import React, { Component } from 'react';
import * as axios from 'axios';
import { Link } from 'react-router-dom';
import Header from '../components/header';
import Card from '../components/card';

export default class TiposConta extends Component {
    constructor(props) {
        super(props)
        this.state = {
            tiposConta: [],
            tiposFilter: []
        }
    }

    UNSAFE_componentWillMount() {
        axios.get('http://localhost:1337/tipoContas', {
            headers: {
                'Authorization': 'banco-vemser-api-fake'
            }
        }).then(res => {
            this.setState({
                tiposConta: res.data.tipos
            })
            this.setState({
                tiposFilter: res.data.tipos
            })
        })
    }
    filtrarTipo(nome) {
        const { tiposConta } = this.state
        const tipo = tiposConta.filter(tipo => tipo.nome.includes(nome.target.value))
        this.setState({
            tiposFilter: tipo
        });
    }

    render() {
        const { tiposFilter } = this.state
        return (
            <div className="container">
                <Header />
                <input placeholder="Informe o tipo de conta" onChange={this.filtrarTipo.bind(this)} />
                {tiposFilter.map(conta =>
                    <Card key={conta.id}>
                        <Link to={{ pathname: `/tipoContas/${conta.id}`, state: { tipoConta: conta } }}  >
                            <h2 key={conta.id}>
                                {conta.nome}
                            </h2>
                        </Link>
                    </Card>
                )}
                
            </div>
        )
    }
}