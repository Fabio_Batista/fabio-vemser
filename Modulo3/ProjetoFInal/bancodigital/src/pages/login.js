import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import '../css/login.css';


export default class TelaLogin extends Component {
  constructor() {
    super();
    this.submit = this.submit.bind(this);
    this.state = {
      email: '',
      password: '',
      access: false
    };
  }

  UNSAFE_componentWillMount() {
    localStorage.removeItem('Token')
    localStorage.removeItem('Username')
  }

  submit = async (e) => {
    const { email, password } = this.state
    await axios.post('http://localhost:1337/login', { email: email, senha: password })
      .then((res) => {
        localStorage.setItem('Token', res.data.token);
        localStorage.setItem('Username', email);
        this.setState({ access: true })
        this.props.history.push("/home");
      }).catch((err) => {
        alert("Dados inválidos!")
      })
  }
  handleChangeEmail(evt) {
    this.setState({ email: evt.target.value });
  }
  handleChangePassword(evt) {
    this.setState({ password: evt.target.value });

  }
  render() {
    return (
      <React.Fragment>
        <div className="title">Dbc Bank</div>
        <div className="wrap">
          Login
        <input name="email" className="input-login" type="text" id="username" placeholder="Email" onBlur={this.handleChangeEmail.bind(this)} />
          <input name="senha" className="input-login" type="password" id="passwor" placeholder="Senha" onBlur={this.handleChangePassword.bind(this)} /> <br></br>
          <Link to="/home" className="button button-purple"> <button onClick={this.submit} className='login'>Logar</button></Link>
        </div>
      </React.Fragment>



    )
  }
}
