import React from 'react';
import '../css/box.css';

export default (props) =>
    <article className="box">
        {props.children}
    </article>
    