import React, { Component } from 'react';
import Header from '../components/header';
import Card from '../components/card';
export default class ContaCliente extends Component {
    render() {
        const { contas } = this.props.location.state
        return (
            <div className="container">
                <React.Fragment key={contas.id}>
                    <Header />
                    <Card>
                        <p>Cliente:</p>
                        <h4>Nome: {contas.cliente.nome}</h4>
                        <h4>Cpf: {contas.cliente.cpf}</h4>
                        <h2>Conta: {contas.tipo.nome}</h2>
                    </Card>
                </React.Fragment>
            </div>
        )
    }
}