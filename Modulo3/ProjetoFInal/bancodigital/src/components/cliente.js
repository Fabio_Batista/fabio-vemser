import React, { Component } from 'react';
import Header from '../components/header';
import Card from '../components/card';

export default class Cliente extends Component {
    render() {
        const { cliente } = this.props.location.state
        return (
            <div className="container">
                <React.Fragment key={cliente.id} >
                    <Header />
                    <Card>
                        <h3>Nome de cliente: {cliente.nome}</h3>
                        <h3>Cpf: {cliente.cpf}</h3>
                        <h3>Agencia:</h3>
                        <h2>Codigo:{cliente.agencia.nome}</h2>
                        <h2>Endereço:</h2>
                        <h2>Endereço: {cliente.agencia.endereco.logradouro}</h2>
                        <p>Numero {cliente.agencia.endereco.numero}</p>
                        <p>Bairro: {cliente.agencia.endereco.bairro}</p>
                        <p>Cidade: {cliente.agencia.endereco.cidade}</p>
                        <p>Uf: {cliente.agencia.endereco.uf}</p>
                    </Card>
                </React.Fragment>
            </div>

        )

    }
}