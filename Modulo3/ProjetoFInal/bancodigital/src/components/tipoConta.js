import React, { Component } from 'react';
import Header from '../components/header';
import Card from '../components/card';

export default class TipoConta extends Component {
    render() {
        const { tipoConta } = this.props.location.state
        return (
            <div className="container">
                <React.Fragment>
                    <Header />
                    <Card>
                        <h2 key={tipoConta.id}>
                            {tipoConta.nome}
                        </h2>
                    </Card>

                </React.Fragment>
            </div>
        )
    }
}