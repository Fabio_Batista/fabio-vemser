
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { logout } from '../api/auth';
import { history } from '../api/history';
import '../css/header.css';
import '../css/grid.css';
import '../css/reset.css';


export default class Header extends Component {

     fazerLogout() {
        logout()
        history.push('/')
        localStorage.removeItem('Token')
        localStorage.removeItem('Username')
    }   
    
    render(){
        return(               
                <div className="main-header">
                    <nav className="container clearfix">
                        <p className="logo" > Dbc Bank</p>
                        <ul className="clearfix">
                            <li><Link className="link" to="/agencias">Agencias</Link></li>                    
                            <li><Link className="link" to="/clientes">Clientes</Link></li>
                            <li><Link className="link" to="/tiposcontas">Tipo de contas</Link></li>
                            <li><Link className="link" to="/contasclientes">Conta de clientes</Link></li>
                            <li><Link className=" logout" to="/" onClick={this.fazerLogout.bind( this )}>Logout</Link></li>
                        </ul>
                    
                    </nav>                    
                </div>
        )
    }
}