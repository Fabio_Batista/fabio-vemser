import React, { Component } from 'react';
import Header from '../components/header';
import Card from '../components/card';

export default class Agencia extends Component {
    constructor ( id, codigo, nome, logradouro, numero, bairro, cidade, uf ){
        super()
        this.id = id
        this.codigo = codigo
        this.nome = nome
        this.logradouro = logradouro
        this.numero = numero 
        this.bairro = bairro
        this.cidade = cidade
        this.uf = uf
    }
    tornarDigital(){
        this.is_digital = true
    }
    
    render() {
        const { agencia } = this.props.location.state
        return (
            <div className="container">
                <React.Fragment key={agencia.id} >
                    <Header/>
                    <Card>
                    <h2>Agencia: {agencia.codigo.value} </h2>
                    <h3>Nome de Agencia: {agencia.nome}</h3>
                    <h4>Endereço: </h4>
                    <p>Logradouro: {agencia.endereco.logradouro}</p>
                    <p>Numero {agencia.endereco.numero}</p>
                    <p>Bairro: {agencia.endereco.bairro}</p>
                    <p>Cidade: {agencia.endereco.cidade}</p>
                    <p>Uf: {agencia.endereco.uf}</p>
                    </Card>
                </React.Fragment>                
            </div>
        )
    }
}