import React, { Component } from 'react';
import './App.css';
import Login from './pages/login';
import Home from './pages/home';

import Agencias from './pages/agencias';
import Agencia from  './components/agencia';

import Clientes from './pages/clientes';
import Cliente from './components/cliente';

import TiposDeContas from './pages/tiposContas';
import Conta from './components/tipoConta';

import ContasCLientes from './pages/contaClientes';
import contaCliente from './components/contaCliente';

import PrivateRoute from './api/PrivateRoute'

import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';


 
export default class App extends Component {

  render() {

    return (
      <Router>
        <Switch>
          <Route exact path="/"  component={ Login } />          
          <PrivateRoute exact path="/home" component={ Home } />

          <PrivateRoute exact path="/agencias" component={ Agencias } />
          <PrivateRoute exact path="/agencia/:id" component={ Agencia } />

          <PrivateRoute exact path="/clientes" component={ Clientes } />
          <PrivateRoute exact path="/cliente/:id" component={ Cliente } />

          <PrivateRoute exact path="/tiposcontas" component={ TiposDeContas } />
          <PrivateRoute exact path="/tipoContas/:id" component={ Conta } />

          <PrivateRoute exact path="/contasclientes" component={ ContasCLientes } />
          <PrivateRoute exact path="/conta/cliente/:id" component={ contaCliente } />
        </Switch>
      </Router>
    );
  }
}









