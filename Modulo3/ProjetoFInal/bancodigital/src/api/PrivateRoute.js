import React from 'react';
import {Route, Redirect } from 'react-router-dom'
import Authenticated from './auth';

 const PrivateRoute = ({component: Component, ...rest}) => (
    <Route {...rest } render={ props => (
      Authenticated() ? 
        <Component { ...props }/>
      : 
        <Redirect to={{ pathname: '/', state: { from: props.location } } } />
      
    )}/>
  )
  export default PrivateRoute