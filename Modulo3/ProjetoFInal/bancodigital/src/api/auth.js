
 const isAuthenticated =  () =>  localStorage.getItem('Token') != null
 const logout = () => localStorage.removeItem('Authorization')

export default  isAuthenticated
export { logout }