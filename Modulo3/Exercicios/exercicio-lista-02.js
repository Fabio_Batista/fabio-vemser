
function cardapioIFood(veggie = true, comLactose = false) {

  const cardapio = [
    'enroladinho de salsicha',
    'cuca de uva'
  ]
  if (
    comLactose) {
    cardapio.push('pastel de queijo')
  }  
  cardapio.push('pastel de carne')
  cardapio.push('empada de legumes marabijosa')

  if (veggie) {
    // TODO: remover alimentos com carne (é obrigatório usar splice!)
    cardapioVeggie = cardapio;
    cardapioVeggie.splice(cardapio.indexOf('enroladinho de salsicha'), 1)
    cardapioVeggie.splice(cardapio.indexOf('pastel de carne'), 1)
  }
  let resultadoFinal = []
  for (let i = 0; i < cardapioVeggie.length; i++) {
    resultadoFinal[i] = cardapioVeggie[i].toUpperCase();

  }
  console.log(resultadoFinal);
  arr = cardapio.concat(cardapioVeggie);
  console.log(arr);
}
cardapioIFood();// esperado: [ 'CUCA DE UVA', 'PASTEL DE QUEIJO', 'EMPADA DE LEGUMES MARABIJOSA' ]
