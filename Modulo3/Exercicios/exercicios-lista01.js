//Exercicio 01

let circulo = {
    raio: 3, 
    tipoCalculo: "C"
}

function calcularCirculo ({raio, tipoCalculo:tipo}){
    return Math.ceil(tipo == "A" ? Math.PI * Math.pow(raio, 2): 2 * Math.PI * raio);    
}
console.log(calcularCirculo(circulo));




//Exercicio 02

/* function naoBissexto(ano){
    return (ano %400 === 0) || (ano %4 === 0 && ano %100 !== 0) ? false : true;
}
console.log(naoBissexto(2020));
 */
/* const teste = {
    diaAula: "Segundo",
    local: "DBC",
    bissexto(){
        return (ano %400 === 0) || (ano %4 === 0 && ano %100 !== 0) ? false : true;
    }
} */
/* console.log(teste.naoBissexto(2020)); */

let naoBissexto = ano => (ano %400 === 0) || (ano %4 === 0 && ano %100 !== 0) ? false : true;

console.log(naoBissexto(2020));
// Exercicio 03



function somarPares(numeros){
    let resultado = 0;
    for (let i = 0; i < numeros.length; i++) {
        if(i % 2 === 0) {
            resultado += numeros[i];
        }
        
    }
   return resultado;
}
somarPares([1, 56, 4.34, 6, -2]);

// Exercicio 04
/* function adicionar(valor1){
    return function (valor2){
        return valor1 + valor2;
    }
}
console.log(adicionar(1)(2)); */

let adicionar = valor1 => valor2 => valor1 + valor2;

console.log(adicionar(3)(4));
console.log(adicionar(5642)(8749));

/* const is_divisivel = (divisor, numero) => !(numero % divisor);
const divisor =2;

console.log(is_divisivel(divisor, 21));
console.log(is_divisivel(divisor, 22)); 
console.log(is_divisivel(divisor, 23));*/

const is_divisivelPor = (divisor, numero) => !(numero % divisor);
/* const is_divisivel = is_divisivelPor(2);

console.log(is_divisivel(21));
console.log(is_divisivel(22)); 
console.log(is_divisivel(23)); */

// Exercicio 05

function imprimirBRL(n){
    let valor = parseFloat(n);
    let casasDecimaisValor = valor % 1;    
    casasDecimaisValor = casasDecimaisValor.toFixed(3);
    console.log(casasDecimaisValor);     
    if(parseInt(casasDecimaisValor[4]) > 0){
        if(valor > 0){
            valor += 0.01;
        } else {
            valor -= 0.01;
        }
        
    }
    console.log( valor.toLocaleString('pt-BR',{style:'currency',currency:'BRL'}));   
}
    imprimirBRL(4.651)
    imprimirBRL(0) // “R$ 0,00”
    imprimirBRL(3498.99) // “R$ 3.498,99”
    imprimirBRL(-3498.99) // “-R$ 3.498,99”
    imprimirBRL(2313477.0135) // “R$ 2.313.477,02”
