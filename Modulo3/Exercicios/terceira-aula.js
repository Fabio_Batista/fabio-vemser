function criarSanduiche(pao, recheio, queijo,salada){
    console.log(`Seu sanduiche tem o pao ${pao} com recheio de ${recheio} e queijo ${queijo} com salada de ${salada}`);
}
const infredientes = [ '3 queijos', 'frango', 'cheddar', "alface"];
//Sanduiche(...infredientes);

function receberValoresIndefinidos(...valores){
    valores.map(valor => console.log(valor));
}

//receberValoresIndefinidos(1, 2, 3, 4, 5, 6)

//console.log([..."Fabio"])

let inputTeste = document.getElementById('campoTeste');
inputTeste.addEventListener('blur',() => {
    alert("Obrigado!")
});