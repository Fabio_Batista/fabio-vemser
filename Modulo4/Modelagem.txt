<?xml version="1.0" encoding="utf-8" ?>
<!-- SQL XML created by WWW SQL Designer, https://github.com/ondras/wwwsqldesigner/ -->
<!-- Active URL: https://ondras.zarovi.cz/sql/demo/?keyword=default -->
<sql>
<datatypes db="postgresql">
  <group label="Numeric" color="rgb(238,238,170)">
    <type label="Integer" length="0" sql="INTEGER" re="INT" quote=""/>
    <type label="Small Integer" length="0" sql="SMALLINT" quote=""/>
    <type label="Big Integer" length="0" sql="BIGINT" quote=""/>
    <type label="Decimal" length="1" sql="DECIMAL" re="numeric" quote=""/>
    <type label="Serial" length="0" sql="SERIAL" re="SERIAL4" fk="Integer" quote=""/>
    <type label="Big Serial" length="0" sql="BIGSERIAL" re="SERIAL8" fk="Big Integer" quote=""/>
    <type label="Real" length="0" sql="BIGINT" quote=""/>
    <type label="Single precision" length="0" sql="FLOAT" quote=""/>
    <type label="Double precision" length="0" sql="DOUBLE" re="DOUBLE" quote=""/>
  </group>

  <group label="Character" color="rgb(255,200,200)">
    <type label="Char" length="1" sql="CHAR" quote="'"/>
    <type label="Varchar" length="1" sql="VARCHAR" re="CHARACTER VARYING" quote="'"/>
    <type label="Text" length="0" sql="TEXT" quote="'"/>
    <type label="Binary" length="1" sql="BYTEA" quote="'"/>
    <type label="Boolean" length="0" sql="BOOLEAN" quote="'"/>
  </group>

  <group label="Date &amp; Time" color="rgb(200,255,200)">
    <type label="Date" length="0" sql="DATE" quote="'"/>
    <type label="Time" length="1" sql="TIME" quote="'"/>
    <type label="Time w/ TZ" length="0" sql="TIME WITH TIME ZONE" quote="'"/>
    <type label="Interval" length="1" sql="INTERVAL" quote="'"/>
    <type label="Timestamp" length="1" sql="TIMESTAMP" quote="'"/>
    <type label="Timestamp w/ TZ" length="0" sql="TIMESTAMP WITH TIME ZONE" quote="'"/>
    <type label="Timestamp wo/ TZ" length="0" sql="TIMESTAMP WITHOUT TIME ZONE" quote="'"/>
  </group>

  <group label="Miscellaneous" color="rgb(200,200,255)">
    <type label="XML" length="1" sql="XML" quote="'"/>
    <type label="Bit" length="1" sql="BIT" quote="'"/>
    <type label="Bit Varying" length="1" sql="VARBIT" re="BIT VARYING" quote="'"/>
    <type label="Inet Host Addr" length="0" sql="INET" quote="'"/>
    <type label="Inet CIDR Addr" length="0" sql="CIDR" quote="'"/>
    <type label="Geometry" length="0" sql="GEOMETRY" quote="'"/>
  </group>
</datatypes><table x="18" y="7" name="PAISES">
<row name="ID" null="0" autoincrement="1">
<datatype>INTEGER</datatype>
<default>NULL</default></row>
<row name="NOME" null="0" autoincrement="0">
<datatype>VARCHAR(50)</datatype>
</row>
<key type="PRIMARY" name="">
<part>ID</part>
</key>
</table>
<table x="162" y="50" name="ESTADOS">
<row name="ID" null="0" autoincrement="1">
<datatype>INTEGER</datatype>
<default>NULL</default></row>
<row name="NOME" null="0" autoincrement="0">
<datatype>VARCHAR(100)</datatype>
<default>'NULL'</default></row>
<row name="ID_PAISES" null="0" autoincrement="0">
<datatype>INTEGER</datatype>
<default>NULL</default><relation table="PAISES" row="ID" />
</row>
<key type="PRIMARY" name="">
<part>ID</part>
</key>
</table>
<table x="330" y="65" name="CIDADES">
<row name="ID" null="0" autoincrement="1">
<datatype>INTEGER</datatype>
<default>NULL</default></row>
<row name="NOME" null="0" autoincrement="0">
<datatype>VARCHAR</datatype>
<default>'NULL'</default></row>
<row name="ID_ESTADOS" null="0" autoincrement="0">
<datatype>INTEGER</datatype>
<default>NULL</default><relation table="ESTADOS" row="ID" />
</row>
<key type="PRIMARY" name="">
<part>ID</part>
</key>
</table>
<table x="765" y="81" name="CLIENTES">
<row name="ID" null="0" autoincrement="1">
<datatype>INTEGER</datatype>
<default>NULL</default></row>
<row name="NOME" null="0" autoincrement="0">
<datatype>VARCHAR(100)</datatype>
</row>
<row name="CPF" null="0" autoincrement="0">
<datatype>CHAR(14)</datatype>
</row>
<key type="PRIMARY" name="">
<part>ID</part>
</key>
</table>
<table x="37" y="298" name="CIDADES_X_CLIENTES">
<row name="ID" null="0" autoincrement="1">
<datatype>INTEGER</datatype>
</row>
<row name="TIPO" null="0" autoincrement="0">
<datatype>INTEGER</datatype>
<default>0</default><comment>0 = RESIDENCIA | 1+ TRABALHO | 2 = REFERENCIA</comment>
</row>
<row name="ID_CIDADES" null="0" autoincrement="0">
<datatype>INTEGER</datatype>
<default>NULL</default></row>
<row name="ID_CLIENTES" null="0" autoincrement="0">
<datatype>INTEGER</datatype>
<default>NULL</default></row>
<key type="PRIMARY" name="">
<part>ID</part>
</key>
</table>
<table x="493" y="84" name="CIDADE-X_CLIENTES">
<row name="ID_CIDADES" null="0" autoincrement="0">
<datatype>INTEGER</datatype>
<default>NULL</default><relation table="CIDADES" row="ID" />
</row>
<row name="ID_CLIENTES" null="0" autoincrement="0">
<datatype>INTEGER</datatype>
<default>NULL</default><relation table="CLIENTES" row="ID" />
</row>
<key type="PRIMARY" name="">
<part>ID_CIDADES</part>
<part>ID_CLIENTES</part>
</key>
</table>
<table x="543" y="405" name="BANCOS">
<row name="ID" null="0" autoincrement="1">
<datatype>INTEGER</datatype>
</row>
<row name="NOME" null="0" autoincrement="0">
<datatype>VARCHAR(50)</datatype>
</row>
<row name="CODIGO" null="0" autoincrement="0">
<datatype>INTEGER(3)</datatype>
</row>
<key type="PRIMARY" name="">
<part>ID</part>
</key>
</table>
<table x="488" y="207" name="AGENCIAS">
<row name="ID" null="0" autoincrement="1">
<datatype>INTEGER</datatype>
</row>
<row name="NOME" null="0" autoincrement="0">
<datatype>VARCHAR</datatype>
</row>
<row name="ID_BANCOS" null="0" autoincrement="0">
<datatype>INTEGER</datatype>
<relation table="BANCOS" row="ID" />
</row>
<row name="ID_CLIENTES" null="0" autoincrement="0">
<datatype>INTEGER</datatype>
<default>NULL</default></row>
<row name="ID_CIDADES" null="0" autoincrement="0">
<datatype>INTEGER</datatype>
<default>NULL</default><relation table="CIDADES" row="ID" />
</row>
<key type="PRIMARY" name="">
<part>ID</part>
</key>
</table>
<table x="732" y="223" name="CONTAS">
<row name="ID" null="0" autoincrement="1">
<datatype>INTEGER</datatype>
</row>
<row name="ID_AGENCIAS" null="0" autoincrement="0">
<datatype>INTEGER</datatype>
<relation table="AGENCIAS" row="ID" />
</row>
<key type="PRIMARY" name="">
<part>ID</part>
</key>
</table>
<table x="953" y="156" name="CLIENTE_X_CONTAS">
<row name="ID" null="0" autoincrement="1">
<datatype>INTEGER</datatype>
</row>
<row name="ID_CLIENTES" null="0" autoincrement="0">
<datatype>INTEGER</datatype>
<default>NULL</default><relation table="CLIENTES" row="ID" />
</row>
<row name="ID_CONTAS" null="0" autoincrement="0">
<datatype>INTEGER</datatype>
<relation table="CONTAS" row="ID" />
</row>
<row name="TIPO" null="0" autoincrement="0">
<datatype>INTEGER(2)</datatype>
<comment>0 = PJ | 1 = PF | 2 = CONJ | 3 = INVEST</comment>
</row>
<key type="PRIMARY" name="">
<part>ID</part>
</key>
</table>
<table x="853" y="353" name="MOVIMENTACOES">
<row name="ID" null="0" autoincrement="1">
<datatype>INTEGER</datatype>
</row>
<row name="ID_CONTAS" null="0" autoincrement="0">
<datatype>INTEGER</datatype>
<relation table="CONTAS" row="ID" />
</row>
<row name="DEBITO" null="0" autoincrement="0">
<datatype>INTEGER</datatype>
<default>NULL</default></row>
<row name="CREDITO" null="0" autoincrement="0">
<datatype>INTEGER</datatype>
</row>
<key type="PRIMARY" name="">
<part>ID</part>
</key>
</table>
</sql>

